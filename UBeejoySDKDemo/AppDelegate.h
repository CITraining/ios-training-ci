//
//  AppDelegate.h
//  UBeejoySDKDemo
//
//  Created by Tanjiatao on 2018/9/30.
//  Copyright © 2018年 Tanjiatao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

