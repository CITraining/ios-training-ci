//
//  AppleOrderDao.h
//  BtgameAppStore
//
//  Created by tanjiatao on 16/4/9.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OneSDKCommonBuglyHelper.h"
#import "BTDbOrderData.h"

@interface AppleOrderDao : NSObject


+(AppleOrderDao *)getInstance;


-(void)initDB;

//把一条交易信息保存到数据库中
-(BOOL)addOrderReceipt:(BTDbOrderData *)paymentResult handlerBlock:(void (^)(NSDictionary *))sqlHandlerresult;

-(void)deleteOrderInDBbyOrderId:(NSString *)orderId;


//得到所有的未提交的交易结果
-(void)getOrderPaymentfromDBsuccessBlock:(void(^)(NSArray *classarray))successHandler
failureBlock:(void(^)(NSDictionary *))failHandler;

@end
