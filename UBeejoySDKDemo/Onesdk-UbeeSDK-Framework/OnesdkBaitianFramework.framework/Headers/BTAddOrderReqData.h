//
//  BTAddOrderReqData.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/22.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTDeviceProperties.h"
#import "BTChargeData.h"
#import "BTArgs.h"

@interface BTAddOrderReqData : NSObject

@property(nonatomic,strong)BTDeviceProperties *deviceProperties;

@property(nonatomic,assign)int oneId;

@property(nonatomic,strong)BTChargeData *chargeData;

@property(nonatomic,strong)BTArgs *extArgs;


@end
