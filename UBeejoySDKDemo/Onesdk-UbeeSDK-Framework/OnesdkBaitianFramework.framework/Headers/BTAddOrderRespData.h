//
//  BTAddOrderRespData.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/19.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTCommonResult.h"
#import "BTOrderInfo.h"
#import "BTArgs.h"

@interface BTAddOrderRespData : NSObject

@property(nonatomic,strong)BTCommonResult *result;

@property(nonatomic,strong)BTOrderInfo *orderInfo;

@property(nonatomic,strong)BTArgs *extData;

@end
