//
//  AppChannelDefine.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/20.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTAppChannelDefine : NSObject

@property(nonatomic,assign)int oneId;

@property(nonatomic,copy)NSString *appId;

@property(nonatomic,copy)NSString *appKey;

@property(nonatomic,copy)NSString *platformId;

@property(nonatomic,copy)NSString *cpName;

@property(nonatomic,copy)NSString *appExt1;

@property(nonatomic,copy)NSString *appExt2;

@property(nonatomic,copy)NSString *appExt3;

@property(nonatomic,copy)NSString *appExt4;

@property(nonatomic,copy)NSString *appExt5;

@end
