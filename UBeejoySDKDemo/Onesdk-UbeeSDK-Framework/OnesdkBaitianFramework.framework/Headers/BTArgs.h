//
//  BTArgs.h
//  BtgameAppStore
//
//  Created by tanjiatao on 16/4/18.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTArgs : NSObject

@property(nonatomic, strong)NSString *arg1;
@property(nonatomic, strong)NSString *arg2;
@property(nonatomic, strong)NSString *arg3;
@property(nonatomic, strong)NSString *arg4;
@property(nonatomic, strong)NSString *arg5;
@property(nonatomic, strong)NSString *arg6;
@property(nonatomic, strong)NSString *arg7;
@property(nonatomic, strong)NSString *arg8;
@property(nonatomic, strong)NSString *arg9;
@property(nonatomic, strong)NSString *arg10;

@end
