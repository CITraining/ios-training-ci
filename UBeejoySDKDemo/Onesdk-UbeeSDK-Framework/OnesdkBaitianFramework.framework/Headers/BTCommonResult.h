//
//  BTCommonResult.h
//  BtgameAppStore
//
//  Created by tanjiatao on 16/3/23.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTCommonResult : NSObject


//返回码 200 -- 成功 ， 201 - 失败 ， 202 --- 系统失败 ， 203 --参数错误
@property(nonatomic, assign)int  resultCode;

//返回码说明
@property(nonatomic, strong)NSString *resultDescr;




//转换Json
-(BTCommonResult *) stringToObject:(NSDictionary *)resultdic;

@end
