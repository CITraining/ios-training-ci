//
//  BTConstans.h
//  BtgameAppStore
//
//  Created by tanjiatao on 16/3/24.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark -
#pragma mark 协议域名
//测试地址//10.17.1.24:8281 10.18.6.142:8080 10.18.6.128:8080  35haoming
#define BTHTTP_URL_TEST @"Local_Test"
//外网测试地址 http://oneserver.100bt.com   https 地址：te-oneserver.100bt.com http://sdk-sg-beta1-zwfz.ubeejoy.com
//#define BTHTTP_URL_OFFICIAL @"http://sdk-sg-beta1-zwfz.ubeejoy.com"
#define BTHTTP_URL_OFFICIAL @"Online_Test"
//发布地址 oneserver.online.100bt.com http://sdk-sg-zwfz.ubeejoy.com https://pe-oneserver.100bt.com
//#define BTHTTP_URL_RELEASE @"http://sdk-sg-zwfz.ubeejoy.com"
#define BTHTTP_URL_RELEASE @"Release"
//版署服务
#define BTHTTP_URL_COPYRIGHTCODE @"http://oneserver.online.100bt.com:8099"


//移动账号的测试地址  10.18.6.190:9999   10.17.1.24:3196   10.18.6.208:8090  10.18.6.32:80
#define BTHTTP_YDCOUNT_URL_TEST @"https://te-ydaccount.100bt.com"
//移动账号的外围测试地址 ydaccount.100bt.com
#define BTHTTP_YDCOUNT_URL_OFFICIAL @"https://te-ydaccount.100bt.com"
//移动账号的正式发布地址 ydaccount.online.100bt.com
#define BTHTTP_YDCOUNT_URL_RELESASE @"https://pe-ydaccount.100bt.com"
//移动账号的版署地址
#define BTHTTP_YDCOUNT_URL_COPYRIGHTCODE @"https://ydaccount.online.100bt.com:442"

#pragma make --- 使用
//移动支付的线上测试地址
#define BTHTTP_YDPAY_URL_OFFICIAL @"https://te-ydpay.100bt.com"
//移动支付的正式发布地址
#define BTHTTP_YDPAY_URL_RELESASE @"https://pe-ydpay.100bt.com"

//云服务域名
#define BTHTTP_ONESDK_URL_CLOUD @"https://cloud-oneserver.100bt.com"
#define BTHTTP_YDACCOUND_URL_CLOUD @"https://cloud-ydaccount.100bt.com"



#pragma mark 请求接口
//移动账号初始化
#define REQUEST_FOR_INIT @"/operate/activity"
//移动账号的登录
#define REQUEST_FOR_LOGIN @"/index/index"
//移动账号中的用户中心
#define REQUEST_FOR_USERCENTER @"/index/index#/page-info"
//BBS的请求id
#define REQUEST_FOR_BBS @"/forum/loginForum/redirect"

//OneSDK初始化id 获取平台参数
#define REQUEST_FOR_ONESDK_INIT @"/common/getAppChannelDefine"

//OneSDK初始化id 获取登陆Session
#define REQUEST_FOR_ONESDK_GETSESSION @"/login/getSession"

//OneSDK提交订单到服务器 验证订单
#define REQUEST_FOR_VERFLYORDER @"/pay/ios_notify_v1"
//OneSDK获取订单结果
#define REQUEST_FOR_ORDERRESULT @"/pay/addOrder"



typedef enum BTClientType{

    BTHttp_For_orderResult = 1,//获取onesdk的order
    BTHttp_For_Init, //移动账号的初始化
    BTHttp_For_VerifyOrder,//订单验证
    BTHttp_For_OneSDKInit,  //onesdk的初始化
    BTHttp_For_YDLogoin,//移动的登录
    BTHttp_For_YDUsercenter,//移动账号的用户中心
    BTHTTp_For_OneSDKSeesion,//获取onesdk的session
    BTHttp_For_BBS//BBS的地址
    
}BTRequestId;


typedef enum BTSDKClienttype{//sdk的类型 是onesdk 还是移动pay
    
   BTONEsdk = 1,
    
   BTYDsdk
    
    
}SdkclienType;


/**
 结果状态
 */
extern NSString * const BaiTian_StatusCode;

/**
 结果描述
 */
extern NSString * const BaiTian_ResultDesc;


@interface BTConstans : NSObject




+(NSString *)getRequestUrl:(SdkclienType)sdkType withRequestId:(BTRequestId)requestId;


@end




/**
 * 初始化时无网络
 */
static NSString * const INIT_FAIL_NO_NETWORK = @"-11";

/**
 * "平台初始化失败"
 */
static NSString * const INIT_FAIL_BY_TARGET_PLATFROM = @"-12";

/**
 * "平台实列化失败"
 */
static NSString * const INIT_FAIL_BY_SDK_NEWINSTANCE = @"-13";


/**
 * 登录时 尚未初始化
 */
static NSString * const LOGIN_FAIL_NO_INIT = @"-21";

/**
 * 支付參數不足
 */
static NSString * const PAY_FAIL_BY_PAYMENTINFO =@"-31" ;
/**
 * 支付參數中回調url地址不符合
 */
static NSString * const PAY_FAIL_BY_NOTICEURL =@"-32"  ;
/**
 * 尚未登錄
 */
static NSString * const PAY_FAIL_BY_NO_LOGIN =@"-33" ;
/**
 * 支付訂單獲取失敗
 */
static NSString * const PAY_FAIL_BY_NO_ORDERID =@"-34" ;

/**
 * 支付失敗 关闭了支付页面
 */
static NSString * const PAY_FAIL_BY_CLOSE_PAYVIEW =@"-35" ;
/**
 * 支付失敗
 */
static NSString * const PAY_FAIL_BY_TARGETSDK =@"-36" ;

/**
 * 苹果商城返回超时
 */
static NSString * const PAY_FAIL_BY_APPSTORE_TIMEOUT =@"-38" ;

/**
 *订单上传失败
 */
static NSString * const PAY_FAIL_BY_UPLOAD_ORDERID =@"-39" ;


/**
 *无支付权限
 */
static NSString * const PAY_FAIL_BY_NO_AUTHORITY =@"-40" ;


/**
 ＊苹果商店中无商品 商品列表为空
 */
static NSString * const PAY_FAIL_BY_NO_PRODUCES_INSTORE =@"-41" ;

/**
 ＊苹果商店中该对应商品
 */
static NSString * const PAY_FAIL_BY_NO_PRODUCE_INSTORE =@"-42" ;
/**
 ＊苹果返回支付凭证为空
 */
static NSString * const PAY_FAIL_BY_APPLEPAY_RECEIPT_NULL =@"-43" ;
/**
 ＊支付参数错误
 */
static NSString * const PAY_FAIL_BY_PAYMENTINFO_ERROR =@"-44" ;
/**
 ＊ipv 验证失败
 */
static NSString * const IPV_NETWORK_FAIL =@"-51" ;

/**
 ＊ipv 验证失败
 */
static NSString * const IPV_NETWORK_TIMEOUT_FAIL =@"-52" ;
    

/** 后端系统下单次数限制 */
static const int PAY_FAIL_BY_PAYCOUNT_LIMIT =2002 ;

/** 后端系统下单商品ID不存在 */
static const int PAY_FAIL_BY_PRODUCTID_ERROR =2001 ;



static NSString * const EVENTID_BEGIN_INIT =@"100001" ;//开始初始化
static NSString * const EVENTID_FINISH_INIT =@"100002" ;//初始化完成
static NSString * const EVENTID_BEGIN_LOGIN =@"100003" ;//开始调用登陆方法
static NSString * const EVENTID_BEGIN_CALL_LOGIN_VIEW =@"100004" ;//开始调用登陆页面
static NSString * const EVENTID_BEGIN_SHOW_LOGIN_VIEW =@"100005" ;//移动账号页面加载开始
static NSString * const EVENTID_LOADING_LOGIN_VIEW =@"100006" ;//正在加载登陆页面
static NSString * const EVENTID_END_LOADING_LOGIN_VIEW =@"100007" ;//结束加载登陆页面
static NSString * const EVENTID_LOGIN_VIEW_LOAD_FAIL =@"100008" ;// 加载登陆页面失败
static NSString * const EVENTID_SDK_LOGIN_FAIL =@"100009" ;//SDK登录失败
static NSString * const EVENTID_SDK_LOGIN_SUCCESS =@"100010" ;//移动账号登陆成功
static NSString * const EVENTID_GETSESSION_FAIL =@"100011" ;
static NSString * const EVENTID_GETSESSION_SUCCESS =@"100012" ;
static NSString * const EVENTID_ENTERGAME=@"100013" ;
static NSString * const EVENTID_CREATEROLE=@"100014" ;
static NSString * const EVENTID_INIT_FAIL = @"100015";//初始化失败



@interface BTErrorCode : NSObject



@end
