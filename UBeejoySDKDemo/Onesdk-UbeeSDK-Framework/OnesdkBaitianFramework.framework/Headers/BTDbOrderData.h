//
//  BTDbOrderData.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/22.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTDbOrderData : NSObject

@property(nonatomic, strong)NSString *orderId;//订单id

@property(nonatomic, strong)NSString *verifyOrderJson;//验证订单的请求json数据

@end
