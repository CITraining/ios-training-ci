//
//  BTDeviceProperties.h
//  BtgameAppStore
//  设备参数类
//  Created by tanjiatao on 16/3/21.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <UIKit/UIKit.h>

@interface BTDeviceProperties : NSObject

@property(nonatomic, strong)NSString *sdkVersion;//sdk版本
@property(nonatomic, strong)NSString *phoneModel;//手机型号
@property(nonatomic, strong)NSString *imsi;//唯一标识
@property(nonatomic, strong)NSString *imei;
@property(nonatomic, strong)NSString *gameVersionName;
@property(nonatomic, strong)NSString *gameVersionCode;
@property(nonatomic, assign)int densityDpi;
@property(nonatomic, assign)int displayScreenWidth;
@property(nonatomic, assign)int displayScreenHeight;
@property(nonatomic, strong)NSString *networkInfo;
@property(nonatomic, strong)NSString *gamepackageName;
@property(nonatomic, strong)NSString *othersdkVersion;
@property(nonatomic, strong)NSString *sdkType;
@property(nonatomic, strong)NSString *sysVersion;
@property(nonatomic, strong)NSString *mac;


-(NSMutableDictionary *)objectToDicyionary;
@end
