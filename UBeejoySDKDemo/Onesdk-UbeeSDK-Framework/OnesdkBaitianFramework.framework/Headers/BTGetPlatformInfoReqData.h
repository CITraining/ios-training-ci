//
//  BTGetPlatformInfoReqData.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/22.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTDeviceProperties.h"

@interface BTGetPlatformInfoReqData : NSObject

@property(nonatomic,strong)BTDeviceProperties *deviceProperties;

@property(nonatomic,assign)int oneId;


@end
