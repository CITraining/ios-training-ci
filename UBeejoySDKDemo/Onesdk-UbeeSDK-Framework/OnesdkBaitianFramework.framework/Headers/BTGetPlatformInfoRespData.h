//
//  BTGetPlatformInfoRespData.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/20.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTCommonResult.h"
#import "BTAppChannelDefine.h"

@interface BTGetPlatformInfoRespData : NSObject

@property(nonatomic,strong)BTCommonResult *result;

@property(nonatomic,strong)BTAppChannelDefine *appChannelDefine;


@end
