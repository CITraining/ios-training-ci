//
//  BTGetSessionReqData.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/22.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTDeviceProperties.h"
#import "BTArgs.h"

@interface BTGetSessionReqData : NSObject

@property(nonatomic,strong)BTDeviceProperties* deviceProperties;

@property(nonatomic,assign)int oneId;

@property(nonatomic,copy)NSString *userId;

@property(nonatomic,copy)NSString *token;

@property(nonatomic,strong)BTArgs *extArgs;

@end
