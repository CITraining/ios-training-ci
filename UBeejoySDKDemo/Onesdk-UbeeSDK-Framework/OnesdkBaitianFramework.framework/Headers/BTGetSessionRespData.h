//
//  BTGetSessionRespData.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/19.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTCommonResult.h"
#import "BTOneUserInfo.h"

@interface BTGetSessionRespData : NSObject

@property(nonatomic,strong)BTCommonResult *result;

@property(nonatomic,copy)NSString *sessionId;

@property(nonatomic,strong)BTOneUserInfo *oneUserInfo;


@end
