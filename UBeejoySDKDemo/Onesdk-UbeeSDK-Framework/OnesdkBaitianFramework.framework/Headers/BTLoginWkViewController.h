//
//  BTLoginWkViewController.h
//  BaitianIosSdk
//
//  Created by tanjiatao01 on 16/9/28.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import <WebKit/WKFoundation.h>
#import "BTConstans.h"



@interface BTLoginWkViewController : UIViewController<WKNavigationDelegate,WKScriptMessageHandler,WKUIDelegate>
@property (nonatomic,strong)WKWebView *btwebview;
@property (nonatomic,assign)BOOL isLogin;
@property(nonatomic, strong)NSTimer *connectTimer;
@property(nonatomic, assign)BOOL bTimeOut;



@end
