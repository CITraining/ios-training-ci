//
//  BTLoginWrapper.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/19.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTUserInfo.h"

@interface BTLoginWrapper : NSObject
@property(nonatomic,copy)void(^successBlock)(NSDictionary *);
@property(nonatomic,copy)void(^failureBlock)(NSDictionary *);


+(instancetype)getInstance;

-(void)initYiDongAccountSDK:(void (^)(NSDictionary *))successHandler failure:(void (^)(NSDictionary *dicinfo))failHandler;

-(void)loginYDaccount:(void (^)(NSDictionary *))successHandler failureBlock:(void (^)(NSDictionary *))failHandler;


-(void)setUserInfo:(BTUserInfo *)userInfo;

-(BTUserInfo *)getUserInfo;

-(void)loginFinish:(BTUserInfo *)userInfo;

-(void)showBBSpage;

-(void)showUserCenter;

-(void)ydsdklogout;

-(void)setLogoutBlock:(void (^)(NSDictionary *dicinfo))successHandler
              failure:(void (^)(NSDictionary *dicinfo))failHandler;

@end
