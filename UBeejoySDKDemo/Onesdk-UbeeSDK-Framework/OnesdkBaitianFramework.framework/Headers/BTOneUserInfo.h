//
//  BTOneUserInfo.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/19.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTArgs.h"

@interface BTOneUserInfo : NSObject

@property(nonatomic,assign)int oneId;

@property(nonatomic,assign)int platformId;

@property(nonatomic,copy)NSString *userId;

@property(nonatomic,copy)NSString *userName;

@property(nonatomic,copy)NSString *nickName;

@property(nonatomic,assign)long createTime;

@property(nonatomic,assign)int overSeaUserType;

@property(nonatomic,strong)BTArgs *extArgs;

@end
