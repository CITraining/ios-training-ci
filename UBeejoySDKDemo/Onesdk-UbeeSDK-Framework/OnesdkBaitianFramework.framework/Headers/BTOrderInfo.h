//
//  BTOrderInfo.h
//  BtgameAppStore
//
//  Created by tanjiatao on 16/4/7.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTOrderInfo : NSObject

@property(nonatomic, strong)NSString *orderId;//订单号
@property(nonatomic, strong)NSString *notifyUrl;//回调地址
@property(nonatomic, strong)NSString *goodsDesc;//商品说明
@property(nonatomic, strong)NSString *currency;//货币名称
@property(nonatomic, assign)int rate;//比例






-(BTOrderInfo *)stringToObeject:(NSDictionary  *)orderdic;













@end
