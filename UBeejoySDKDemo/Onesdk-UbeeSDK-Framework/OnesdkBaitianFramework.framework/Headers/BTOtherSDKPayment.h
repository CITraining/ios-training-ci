//
//  BTOtherSDKPayment.h
//  OnesdkBaitian2.0
//
//  Created by Tanjiatao on 2017/8/2.
//  Copyright © 2017年 zengzhifeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTSdkPaymentInfo.h"
#import "BTOrderInfo.h"
#import "BTArgs.h"
@interface BTOtherSDKPayment : NSObject

@property(nonatomic, strong)BTSdkPaymentInfo *btsdkPaymentInfo;
@property(nonatomic, strong)BTOrderInfo *btOrderInfo;
@property(nonatomic, strong)BTArgs *btArgs;
@property(nonatomic, strong) void (^PaysuccessBlock)(NSDictionary *dicinfo);
@property(nonatomic, strong) void (^PayfailureBlock)(NSDictionary *dicinfo);


@end
