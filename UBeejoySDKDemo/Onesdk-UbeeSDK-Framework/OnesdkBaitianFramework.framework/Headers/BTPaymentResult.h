//
//  BTPaymentResult.h
//  (NSString *)json
//
//  Created by tanjiatao on 16/4/8.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface BTPaymentResult : NSObject



@property(nonatomic, strong)NSString *orderId;//订单id
@property(nonatomic, strong)NSString *orderStatus;//订单状态码
@property(nonatomic, strong)NSString *orderDescr;//订单说明
@property(nonatomic, strong)NSString *outOrderId;
@property(nonatomic, strong)NSString *receipt;//订单凭证
@property(nonatomic, assign)int isSandBox;// 1是 0否
@property(nonatomic,assign)int isSimulator;// 1是 0否


-(NSString *)objectToJsonString;



@end
