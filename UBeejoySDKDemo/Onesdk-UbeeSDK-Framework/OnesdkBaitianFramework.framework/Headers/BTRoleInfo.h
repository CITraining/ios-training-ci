//
//  BTRoleInfo.h
//  Btgame_OneSDK_9splay
//
//  Created by ywzc-2016A on 17/3/30.
//  Copyright © 2017年 zengzhifeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTRoleInfo : NSObject

@property(nonatomic, strong)NSString *roleName;
@property(nonatomic, strong)NSString *roleId;
@property(nonatomic, strong)NSString *serverName;
@property(nonatomic, strong)NSString *serverId;

@property int roleLevel;

@property NSString *roleCTime;
@property int vipLevel;
@property int isVIP;
@property(nonatomic, strong)NSString *loginTime;

@end


static const NSString *ROLE_NAME = @"roleName";
static const NSString *ROLE_ID = @"roleId";
static const NSString *SERVER_NAME = @"serverName";
static const NSString *SERVER_ID = @"serverId";
static const NSString *ROLE_LEVEL = @"roleLevel";

static const NSString *ROLECREATE_TIME = @"roleCTime";
static const NSString *ROLE_VIPLevel = @"vipLevel";
static const NSString *ROLE_ISVIP = @"isVip";
static const NSString *ROLE_LOGINTIME = @"loginTime";
