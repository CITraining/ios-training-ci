//
//  BTSdkLoginCallback.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/19.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTArgs.h"


@interface BTSdkLoginCallback : NSObject

@property(nonatomic,copy)NSString *userId;

@property(nonatomic,copy)NSString *token;

@property(nonatomic,strong)BTArgs *args;

@end
