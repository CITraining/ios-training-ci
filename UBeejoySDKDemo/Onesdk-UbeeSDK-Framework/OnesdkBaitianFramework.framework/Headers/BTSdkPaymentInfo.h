//
//  BTSdkPaymentInfo.h
//  OnesdkBaitian2.0
//
//  Created by ywzc-2016A on 17/6/27.
//  Copyright © 2017年 zengzhifeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTChargeData.h"

@interface BTSdkPaymentInfo : NSObject

@property(nonatomic, strong)NSString *userId;//用户Id
@property(nonatomic, strong)NSString *roleId;//角色Id
@property(nonatomic, strong)NSString *roleName;//角色名称
@property(nonatomic, strong)NSString *roleLevel;//角色等级
@property(nonatomic, strong)NSString *serverId;//服务器Id
@property(nonatomic, strong)NSString *serverName;//服务器名称
@property(nonatomic, strong)NSString * money;//付费金额    单位：元
@property(nonatomic, strong)NSString *callBackInfo;//透传字段
@property(nonatomic, strong)NSString *goodsDesc;//商品描述
@property(nonatomic, strong)NSString *appproductId;//Bank里面的商户Id
@property(nonatomic, strong)NSString *goodsId;
@property(nonatomic, strong)NSString *outOrderNo;
@property(nonatomic, strong)NSString *filterParam;
@property(nonatomic, strong)NSString *zoneId;//分区ID
@property(nonatomic, assign)BOOL isiosChecking;


-(BTChargeData*)transformToChargeData;

-(BOOL) checkSdkPayment;

@end
