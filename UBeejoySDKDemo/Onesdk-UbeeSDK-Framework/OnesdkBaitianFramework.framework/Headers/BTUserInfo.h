//
//  BTUserInfo.h
//  BtgameAppStore
//
//  Created by tanjiatao on 16/4/15.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTUserInfo : NSObject

@property(nonatomic, strong)NSString *account;//用户名称
@property(nonatomic, strong)NSString *token;//登陆token
@property(nonatomic, strong)NSString *identity;//唯一标识
@property(nonatomic, strong)NSString *baitianId;
@property(nonatomic, strong)NSString *gameId;
@property(nonatomic, strong)NSString *channelId;
@property(nonatomic, strong)NSMutableDictionary *ext;

//转换Json
-(NSString *) objectToJsonString;

@end
