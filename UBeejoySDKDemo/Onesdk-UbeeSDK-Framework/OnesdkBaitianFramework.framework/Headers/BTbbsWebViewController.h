//
//  BTbbsWebViewController.h
//  BtgameAppStore
//
//  Created by tanjiatao on 16/7/13.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface BTbbsWebViewController : UINavigationController<UIWebViewDelegate>
@property (nonatomic,strong)UIWebView *btwebview;
@property (nonatomic,strong)UIBarButtonItem *backwardBarItem;
@property (nonatomic,strong)UIBarButtonItem *forwardBarItem;
@property (nonatomic,strong)UIBarButtonItem *refreshBarItem;
@property (nonatomic,strong)UIBarButtonItem *openOutBarItem;
@property (nonatomic,strong)UIBarButtonItem *closeBarItem;


@end
