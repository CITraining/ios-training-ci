//
//  BTsdkManager.h
//  BtgameAppStore
//
//  Created by tanjiatao on 16/3/22.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BTsdkparmersProfile.h"
#import "BTOneUserInfo.h"
#import "SDKBusinessDelegate.h"
#import "BTDeviceProperties.h"
#import "DevicePropertiesSa.h"

static NSString *PLIST_SHAREPARAMERS_KEY=@"ShareParamers";//info.plist表中的分享参数的key
static NSString *PLIST_BAITIANPARAMERSKEY_KEY=@"BaitianParamers";//info.plist表中的Baitian参数的key
static NSString *PLIST_BAITIANGAMEIDKEY_KEY=@"BtGameId";//info.plist表中的Baitian参数的key
static NSString *PLIST_BAITIANONEIDKEY_KEY=@"BtOneId";//info.plist表中的Baitian参数的key
static NSString *PLIST_BAITIANCHANNELIDKEY_KEY=@"BtChannelId";//info.plist表中的Baitian参数的key 二级渠道ID
static NSString *PLIST_BAITIANPROFORMID_KEY=@"BtPlatformId";//info.plist表中的Baitian参数的key 平台ID
static NSString *PLIST_BAITIANERRORDIALOG_KEY=@"isShowErrorMsgDialog";//info.plist表中的Baitian参数的key 是否使用sdk错误弹框
static NSString *PLIST_BAITIANPAYCONUTLIMITERRORMESSAGE_KEY=@"BtPayCountLimitErrorMsg";//info.plist表中的Baitian参数的key 限制充值次数的错误信息

//切换网络时候 需要查找数据库未提交的订单 然后再进行订单提交后台
@interface BTsdkManager : NSObject


@property(nonatomic,strong)id<SDKBusinessDelegate> businessDelegate;

/**
 *是否上线AppStore  默认为NO
 */
@property(nonatomic, assign)BOOL bAppStore;

/**
 *内购是否是沙箱测试环境 bAppStore为YES时有效 默认是NO
 */
@property(nonatomic, assign)BOOL ISSandbox;

@property(nonatomic,strong)NSString *gameId;

@property(nonatomic,assign)int oneId;

@property(nonatomic,strong)NSString *channelId;

@property(nonatomic,assign)int platformId;

@property(nonatomic,assign)BOOL isShowErrorMsgDialog;

@property(nonatomic,assign)NSString *payCountLimitErrorMsg;

//deviceOrientation
@property(nonatomic, assign)UIDeviceOrientation currentDeviceOrientation;
/**
 *应用程序所支持的设备方向，xxxx-info.plist 中所设置的方向,
 *当SDK方向设置为 EXXWanInterfaceOrientationNone 时可以设置，且此时的
 *默认值为 UIInterfaceOrientationMaskAll
 */
@property(nonatomic, assign)UIInterfaceOrientationMask appInterfaceOritationMask;


//+ (BTsdkManager *) getInstanceWithIsAppStore:(BOOL)isAppStore
//                                   IsSandbox:(BOOL)isSandbox;

/**
 *  获取SDK实例
 */
+ (BTsdkManager *) shareSDK;

/**
 支付接口，
 payment: 支付所需的参数，请参照例子与文档
 success:支付操作完成的block回调
 */
- (void)payWithPaymentInfo:(BTSdkPaymentInfo *)payment
                   success:(void (^)(NSDictionary *dicinfo))successHandler
                   failure:(void (^)(NSDictionary *dicinfo))failHandler;

/**
 支付接口，
 parameters: 支付所需的参数，请参照例子与文档
 success:支付操作完成的block回调
 */
- (void)payWithParameters:(NSDictionary *)parameters
                  success:(void (^)(NSDictionary *dicinfo))successHandler
                  failure:(void (^)(NSDictionary *dicinfo))failHandler;


-(void)initSDK:(void (^)(NSDictionary *dicinfo))successHandler
              failure:(void (^)(NSDictionary *dicinfo))failHandler;


-(void)loginSDK:(void (^)(NSDictionary *dicinfo))successHandler
        failure:(void (^)(NSDictionary *dicinfo))failHandler;


-(void)showUsercenter;

-(void)showBBSpage;

-(NSString *)getCurrentUserId;

-(void)exitSDK:(void (^)(NSDictionary *dicinfo))successHandler
       failure:(void (^)(NSDictionary *dicinfo))failHandler;

-(void)setLogoutBlock:(void (^)(NSDictionary *dicinfo))successHandler
              failure:(void (^)(NSDictionary *dicinfo))failHandler;

-(void)logoutSDK;

-(void)sharewithImgs:(NSArray *)imgs
         contenttext:(NSString *)text
               title:(NSString *)title
                 url:(NSString *)url
                type:(ShareType)shareType
             success:(void (^)(NSDictionary *dicinfo))successHandler
             failure:(void (^)(NSDictionary *dicinfo))failHandler;


-(void)destorySDK;

+(int) getPlatFormId;

+(int)getRunningType;


/**
 初始化接口
 @gameId  游戏ID
 @channelId 渠道ID
 @finishHandler：初始化结果的回调
 */
-(void)initSDKWithGameID:(NSString *)gameId atChannelId:(NSString *)channelID
                 success:(void (^)(NSDictionary *dicinfo))successHandler
                 failure:(void (^)(NSDictionary *dicinfo))failHandler;

//升级游戏角色
-(void)upgradRole:(NSDictionary *)roleInfo;


//新建游戏角色
-(void)createRole:(NSDictionary *)roleInfo;

//进入游戏
-(void)enterGame:(NSDictionary *)roleInfo;

-(BTRoleInfo*)getRoleInfo;

//获取手机设备的json串
-(NSString *)getDevJson;


-(BOOL)isShowUserCenterButton;

-(void)setUserInfo:(BTOneUserInfo*) oneUserInfo;

-(BTOneUserInfo *)getUserInfo;

+ (UIViewController *)appRootController;

-(BOOL)isLogin;

-(void)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

-(void)applicationDidBecomeActive:(UIApplication *)application ;

- (void)applicationWillTerminate:(UIApplication *)application ;

- (void)application:(UIApplication*)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;

- (BOOL) application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void
                                                                        (^_Nonnull)(NSArray *_Nullable))restorationHandler;

- (void)rateUS;

- (void) onFinishNewRoleTutorial;

- (void) onObtainNewRolePack;

//用于后端统计的GameID
- (NSString *)getGameID;
- (void) reportShareResult:(NSString *)shareType shareDes:(NSString *)shareDes shareResult:(int)shareRes;

- (NSString *)getDeviceProperties;

@end
