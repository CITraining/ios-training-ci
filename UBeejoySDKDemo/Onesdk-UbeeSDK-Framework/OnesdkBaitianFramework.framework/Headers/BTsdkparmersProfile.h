//
//  BTsdkparmersProfile.h
//  BaitianApplePay
//
//  Created by tanjiatao on 16/5/20.
//  Copyright © 2016年 baitian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTsdkparmersProfile : NSObject

/**
 session id
 */
extern NSString * const BT_SESSION_ID;

//平台id
extern NSString * const BT_PLATFROM_Id;

//userId  用户id
extern NSString * const BT_USERID;

//roleId  角色id
extern NSString * const BT_PAY_ROLEID;

//roleName 角色名称
extern NSString * const BT_PAY_ROLENAME;

//roleLevele 角色等级
extern NSString * const BT_PAY_ROLEVEL;

//serverId  服务器Id
extern NSString * const BT_PAY_SERVERID;

//serverName  服务器名称
extern NSString * const BT_PAY_SERVERNAME;

//money  金额
extern NSString * const BT_PAY_MONEY;

//callbackInfo  透传字典
extern NSString * const BT_PAY_CALLBACKINFO;

//goodsDesc  商品说明
extern NSString * const BT_PAY_GOODSDESC;

//goodsID  商品ID
extern NSString * const BT_PAY_GOODSID;

//appleproductId  apple商品ID
extern NSString * const BT_PAY_APPLEPRODUCT_ID;

//exStr
extern NSString * const BT_PAY_EXSTR;

//zoneid 分区ID
extern NSString * const BT_PAY_ZONEID;

extern NSString * const BT_PAY_ISIOSCHECKING;

//outorderNo
extern NSString * const BT_PAY_OUTORDERNO;

//resultCode   结果码
extern NSString * const BT_RESULTCODE;


//resultDesc 结果说明
extern NSString * const BT_RESULTDESC;

extern NSString * const SHARE_ID;

extern NSString * const WX_APPID;


extern NSString * const WX_APPKEY;

extern NSString * const QQ_APPID;


extern NSString * const QQ_APPKEY;

extern NSString * const WB_APPID;


extern NSString * const WB_APPKEY;

extern NSString * const WB_URL;

extern NSString * const SHARE_TYPE;


typedef enum Share_Type{//sdk的类型 是onesdk 还是移动pay
    
    SHARE_IMG = 1,
    
    SHARE_WEB =2,
    
    
    SHARE_TEXT
    
    
}ShareType;




@end
