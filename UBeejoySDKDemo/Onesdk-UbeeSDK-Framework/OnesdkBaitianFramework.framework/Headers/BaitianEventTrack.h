//
//  BaitianEventTrack.h
//  BaitianEventTrack
//
//  Created by Tanjiatao on 2017/4/28.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface BaitianEventTrack : NSObject

+(BaitianEventTrack *)shareInstance;

-(void)trackBaseEvent:(NSString *)eventId extDictionary:(NSMutableDictionary *)extDictionary;

-(void)trackAccountEvent:(NSString *)eventId accountId:(NSString *)accountId
           extDictionary:(NSMutableDictionary *)extDictionary;



@end
