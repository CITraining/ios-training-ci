//
//  DataRequest.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/15.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTArgs.h"
#import "BTChargeData.h"
#import "BTOneUserInfo.h"
#import "BTGetSessionRespData.h"
#import "BTAddOrderRespData.h"
#import "BTGetPlatformInfoRespData.h"
#import "BTConstans.h"

@interface DataRequest : NSObject


+(instancetype)shareInstance;

//平台初始化
-(void)getAppPlatformInfoWithOneId:(int)oneId success:(void(^)(BTGetPlatformInfoRespData *infoRespData))successBlock fail:(void(^)(NSDictionary *resultDic))failBlock;


//通过token获取session
-(void)getSessionWithOneId:(int)oneId userId:(NSString *)userId token:(NSString *)token args:(BTArgs *)args success:(void(^)(BTGetSessionRespData *sessionRespData))successBlock fail:(void(^)(NSDictionary *resultDic))failBlock;


//下单获取orderId
-(void)addOrderWithOneId:(int)oneId chargeData:(BTChargeData*) chargeData args:(BTArgs*)args success:(void(^)(BTAddOrderRespData *orderRespData))successBlock fail:(void(^)(NSDictionary *resultDic))failBlock;

//请求返回带{result:{resultCode:xx,resultDescr:xx}}格式的处理
-(void)sendCommonRequest:(NSMutableDictionary *)reqDic url:(NSString *)url success:(void(^)(NSDictionary *resultDic))successBlock fail:(void(^)(NSDictionary *resultDic))failBlock;

//普通的请求,适用任何格式
-(void)sendRequest:(NSMutableDictionary *)reqDic url:(NSString *)url success:(void(^)(NSDictionary *resultDic))successBlock fail:(void(^)(NSDictionary *resultDic))failBlock;

-(NSString *)geturlbyRequseType:(BTRequestId)requestId;

@end
