//
//  DevicePropertiesSa.h
//  OnesdkBaitianFramework
//
//  Created by Tanjiatao on 2018/9/21.
//  Copyright © 2018年 zengzhifeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <OnesdkBaitianFramework/OnesdkBaitianFramework.h>


@interface DevicePropertiesSa : NSObject

@property(nonatomic, strong)NSString *dataAppId; //gameId

@property(nonatomic, strong)NSString *terminal; //终端id, android传0

@property(nonatomic, strong)NSString *platformId; //平台id

@property(nonatomic, strong)NSString *imsi; //sim卡唯一标识

@property(nonatomic, strong)NSString *imei; //设备唯一标识

@property(nonatomic, strong)NSString *platformIdSecond; //二级平台id

@property(nonatomic, strong)NSString *phoneModel; //手机型号

@property(nonatomic, strong)NSString *phoneSysVersion; //手机操作系统版本

@property(nonatomic, strong)NSString *densityDpi; //屏幕密度

@property(nonatomic, strong)NSString *displayScreenWidth; //屏幕宽度

@property(nonatomic, strong)NSString *displayScreenHeight; //屏幕高度

@property(nonatomic, strong)NSString *networkInfo; //网络类型/WiFi/2G/3G

@property(nonatomic, strong)NSString *gameVersionCode; //游戏版本号
@property(nonatomic, strong)NSString *gameVersionName;
@property(nonatomic, strong)NSString *gamepackageName;//游戏包名


@end


