//
//  IAPWrapper.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/19.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>
#import "BTOrderInfo.h"
#import "BTChargeData.h"
#import "BTSdkPaymentInfo.h"




static NSString * const TRANSACTION_RECEIPT_KEY = @"receipt";
static NSString * const TRANSACTION_ORDERID_KEY = @"orderId";
static NSString * const TRANSACTION_ORDERAMOUNT_KEY = @"amount";
//通知服务器支付成功的说明
static NSString * const PAY_SUCCESS = @"200";
static NSString * const PAY_SUCCESS_DESC = @"支付成功";
//通知客户端支付成功的说明
static NSString * const PAY_SUCCESS_TOGAME = @"0";




/**  该用户无购买权利，如地区限制，沙盒账号不对等原因*/
static NSString * const PAY_FAIL_BY_STORTKIT_NOPROMISSION = @"3001";
static NSString * const PAY_FAIL_BY_STORTKIT_NOPROMISSION_DESC = @"当前苹果账户无法购买商品(如有疑问，可以询问苹果客服)";
/**  无法从Store Kit中获取苹果商品ID*/
static NSString * const PAY_FAIL_BY_STORTKIT_NOPRODUCT = @"3002";
static NSString * const PAY_FAIL_BY_STORTKIT_NOPRODUCT_DESC = @"无法从Store Kit中获取苹果商品ID*";
/**  Store Kit中获取到的商品ID与客户端传入的不一致*/
static NSString * const PAY_FAIL_BY_STORTKIT_ERRORPRODUCTID = @"3003";
static NSString * const PAY_FAIL_BY_STORTKIT_ERRORPRODUCTID_DESC = @"Store Kit中获取到的商品ID与客户端传入的不一致";
/**  从transaction中获取订单信息与receipt时异常*/
static NSString * const PAY_FAIL_BY_STORTKIT_GET_RECEIPT_EXCEPTION = @"3004";
static NSString * const PAY_FAIL_BY_STORTKIT_GET_RECEIPT_EXCEPTION_DESC = @"从transaction中获取订单信息与receipt时异常";

static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorUnknown = @"3011";
static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorUnknown_DESC = @"支付失败：未知错误，可能使用越狱手机，或下载途径非苹果商店,或非沙盒账号";

static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorClientInvalid = @"3012";
static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorClientInvalid_DESC= @"支付失败：当前苹果账号无法购买商品";

static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorPaymentCancelled = @"3013";
static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorPaymentCancelled_DESC = @"支付失败：订单已取消";

static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorPaymentInvalid = @"3014";
static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorPaymentInvalid_DESC = @"支付失败：订单无效(如有疑问，可以询问苹果客服)";

static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorPaymentNotAllowed = @"3015";
static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorPaymentNotAllowed_DESC = @"支付失败：当前苹果设备无法购买商品(如有疑问，可以询问苹果客服)";

static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorStoreProductNotAvailable = @"3016";
static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorStoreProductNotAvailable_DESC = @"支付失败：当前商品不可用";

static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorCloudServicePermissionDenied = @"3017";
static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorCloudServicePermissionDenied_DESC = @"支付失败：无权限链接苹果云服务";
/**  支付失败：设备无法连接到云服务*/
static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorCloudServiceNetworkConnectionFailed = @"3018";
static NSString * const PAY_FAIL_BY_STORTKIT_SKErrorCloudServiceNetworkConnectionFailed_DESC = @"支付失败：设备无法连接到云服务";
/**  支付失败：其他未知错误*/
static NSString * const PAY_FAIL_BY_STORTKIT_DEFAULTERROR = @"3019";
static NSString * const PAY_FAIL_BY_STORTKIT_DEFAULTERROR_DESC = @"支付失败：其他未知错误";



static NSString * const PAY_PROCESS_CANPAY = @"4001";
static NSString * const PAY_PROCESS_CANPAY_DESC = @"支付过程：可以使用苹果内购";

static NSString * const PAY_PROCESS_GETPRODUCEID_FROMSTOREKIT = @"4002";
static NSString * const PAY_PROCESS_GETPRODUCEID_FROMSTOREKIT_DESC = @"支付过程：成功在App Store Kit中获取注册的商品ID";

static NSString * const PAY_PROCESS_RIGHTPRODUCE = @"4003";
static NSString * const PAY_PROCESS_RIGHTPRODUCE_DESC = @"客户端的商品ID与苹果后台注册的ID一致";

static NSString * const PAY_PROCESS_PURCHASE =@"4004";
static NSString * const PAY_PROCESS_PURCHASE_DESC =@"支付过程：发起内购";

static NSString * const PAY_PROCESS_GET_PURCHASE_RUNNING= @"4005";
static NSString * const PAY_PROCESS_GET_PURCHASE_RUNNING_DESC= @"支付过程：支付进行中";

static NSString * const PAY_PROCESS_GET_PURCHASERESULTSUCCESS = @"4006";
static NSString * const PAY_PROCESS_GET_PURCHASERESULTSUCCESS_DESC = @"支付过程：收到内购成功结果";

static NSString * const PAY_PROCESS_GET_PURCHASERESULTFAIL = @"4007";
static NSString * const PAY_PROCESS_GET_PURCHASERESULTFAIL_DESC = @"支付过程：收到内购失败结果";


@interface IAPWrapper : NSObject<SKPaymentTransactionObserver,SKProductsRequestDelegate>
@property(nonatomic, strong)NSTimer *connectTimer;
@property(nonatomic, assign)BOOL bTimeOut;
@property (nonatomic,strong) NSString *requestUpLoadUrl;

+(instancetype)getInstance;
//使用苹果内购，内购结果通过Block回调出去
-(void)payWithAppStore:(BTOrderInfo *)orderinfo and:(BTSdkPaymentInfo *) paymentInfo
          successBlock:(void(^)(NSDictionary *))successHandler
          failureBlock:(void(^)(NSDictionary *))failHandler;

//初始化苹果内购组件 尽早调用
-(void)initPay;

-(void)addPayObserver;

-(void)removePayObserver;

-(void)supportOrder;//数据库订单重发

@end

