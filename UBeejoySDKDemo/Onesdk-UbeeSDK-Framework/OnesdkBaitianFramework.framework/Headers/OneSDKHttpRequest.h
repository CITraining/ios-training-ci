//
//  OneSDKHttpRequest.h
//  Taiyou_YuJun_OneSDK
//
//  Created by Tanjiatao on 2017/2/12.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>


static const NSString * HTTP_RESULT_CODE = @"resultCode";
static const NSString * HTTP_RESULT_CONTENT= @"resultContent";

/**
 * 网络连接报错
 */
static NSString *NETWORK_ERROR = @"-200";
static NSString *RequestType_POST = @"POST";
static NSString *RequestType_GET = @"GET";
/**
 * 网络返回数据为空
 */
static NSString * const NETWORK_DATA_NULL = @"-201";
static NSString * const RESPONE_NULL = @"-202";
static NSString * const REQUEST_ERROR = @"-203";


@interface OneSDKHttpRequest : NSObject


/**
 * post请求 带数据
 **/
-(void)httpRequestAsync:(NSString *)url jsonData:(NSData *)jsonData
                success:(void (^)(NSDictionary *dicinfo))successHandler
                failure:(void (^)(NSDictionary *dicinfo))failHandler;

/**
 * post请求 带数据 带header
 **/
-(void)httpRequestAsyncWithCustomHeader:(NSMutableDictionary *)header requestUrl:(NSString *)url jsonData:(NSData *)jsonData success:(void (^)(NSDictionary *))successHandler failure:(void (^)(NSDictionary *))failHandler;
/**
 * 普通的post请求 无数据
 **/
-(void)httpRequestAsync:(NSString *)url success:(void (^)(NSDictionary *))successHandler failure:(void (^)(NSDictionary *))failHandler;

/**
 * 请求参数以拼接方式放入body
 **/
-(void)httpRequestAsync:(NSString *)url param:(NSDictionary *)paramDic success:(void (^)(NSDictionary *))successHandler failure:(void (^)(NSDictionary *))failHandler;

@end
