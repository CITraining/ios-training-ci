//
//  SDKBusiness.h
//  BaitianIosSdk
//
//  Created by ywzc-2016A on 17/6/15.
//  Copyright © 2017年 tanjiatao. All rights reserved.
//
#import "BTRoleInfo.h"
#import "BTChargeData.h"
#import "BTOrderInfo.h"
#import "BTSdkLoginCallback.h"
#import "BTSdkPaymentInfo.h"
#import <UIKit/UIKit.h>

@protocol SDKBusinessDelegate <NSObject>

-(void)doTargetSDKInitSuccess:(void (^)(NSDictionary *))successHandler failure:(void (^)(NSDictionary *dicinfo))failHandler;

-(void)doTargetSDKLoginSuccess:(void (^)(BTSdkLoginCallback *dicinfo))successHandler failure:(void (^)(NSDictionary *dicinfo))failHandler;

-(void)doTargetSDKPay:(BTSdkPaymentInfo *)paymentInfo orderInfo:(BTOrderInfo *)orderInfo extArgs:(BTArgs *)args success:(void (^)(NSDictionary *dicinfo))successHandler failure:(void (^)(NSDictionary *dicinfo))failHandler;

-(void)doTargetSDKLogoutSuccess:(void (^)(NSDictionary *dicinfo))successHandler failure:(void (^)(NSDictionary *dicinfo))failHandler;

-(void)setLogoutBlock:(void (^)(NSDictionary *dicinfo))successHandler
              failure:(void (^)(NSDictionary *dicinfo))failHandler;

-(void)sharewithImgs:(NSArray *)imgs
         contenttext:(NSString *)text
               title:(NSString *)title
                 url:(NSString *)url
                type:(ShareType)shareType
             success:(void (^)(NSDictionary *dicinfo))successHandler
             failure:(void (^)(NSDictionary *dicinfo))failHandler;


-(BOOL)isShowUserCenterButton;

-(void)showUserCenter;

-(void)destroySDK;

-(void)enterGame:(BTRoleInfo *)btRoleInfo;

-(void)upgradeRole:(BTRoleInfo *)btRoleInfo;

-(void)createRole:(BTRoleInfo *)btRoleInfo;

-(void)showBBS;

-(void)rateUS;

- (void) onFinishNewRoleTutorial;

- (void) onObtainNewRolePack;

-(void)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

-(void)applicationDidBecomeActive:(UIApplication *)application ;

- (void)applicationWillTerminate:(UIApplication *)application ;

- (void)application:(UIApplication*)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;

- (BOOL) application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void
                                                                        (^_Nonnull)(NSArray *_Nullable))restorationHandler;


- (void) reportShareResult:(NSString *)shareType shareDes:(NSString *)shareDes shareResult:(int)shareRes;

@end



