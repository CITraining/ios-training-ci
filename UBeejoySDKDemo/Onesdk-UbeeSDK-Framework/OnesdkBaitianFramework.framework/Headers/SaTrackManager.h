//
//  SaTrackManager.h
//  OnesdkBaitianFramework
//
//  Created by Tanjiatao on 2017/10/17.
//  Copyright © 2017年 zengzhifeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SaTrackManager : NSObject

+ (SaTrackManager *) shareInstance;

- (void)sensorsTrack:(NSString *)eventId isFlush:(bool)isflush;

- (void)sensorsTrack:(NSString *)eventId withProperties:(NSDictionary *)paramers isFlush:(bool)isflush;

- (void)sensorsTrack:(NSString *)event;
    
- (void)sensorsTrack:(NSString *)event withProperties:(nullable NSDictionary *)propertyDict;

@end
