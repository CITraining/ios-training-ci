//
//  OtherPartySDKBussiness.h
//  OnesdkBaitian2.0
//
//  Created by Tanjiatao on 2017/8/1.
//  Copyright © 2017年 zengzhifeng. All rights reserved.
//
//7k
#import <Foundation/Foundation.h>
#import <OnesdkBaitianFramework/OnesdkBaitianFramework.h>
#import <OnesdkBaitianFramework/BTOtherSDKPayment.h>
#import <OnesdkBaitianFramework/SDKBusinessDelegate.h>
#import "BtSeaSDKManager.h"
@interface BaitianSDKBusiness : NSObject<SDKBusinessDelegate,BtSeaSDKManagerDelegate>


@end
