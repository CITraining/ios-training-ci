//
//  LHDataBaseExecute.h
//  LHNetWorking
//
//  Created by zhao on 16/1/8.
//  Copyright © 2016年 李浩. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LHDataBaseExecute : NSObject

- (instancetype)initWith:(NSString*)sqlPath;

- (BOOL)writeExecuteWith:(NSString*)sqlString handlerBlock:(void (^)(NSDictionary *))sqlHandlerresult;

- (void)readExecuteWith:(NSString*)sqlString tableName:(NSString*)tableName success:(void(^)(NSArray* resultArray))success faild:(void(^)(NSError* error))faild;

- (void)readExecuteWith:(NSString *)sqlString tableName:(NSString *)tableName WithClass:(Class)class success:(void (^)(NSArray *))success faild:(void (^)(NSError *))faild;

@end
