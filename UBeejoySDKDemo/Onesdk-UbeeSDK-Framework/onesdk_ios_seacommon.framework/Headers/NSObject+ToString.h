//
//  NSObject+ToString.h
//  onesdk-ios-seacommon
//
//  Created by Tanjiatao on 2018/10/25.
//  Copyright © 2018年 zengzhifeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (ToString)
-(NSMutableDictionary *)toDictionary;
@end


