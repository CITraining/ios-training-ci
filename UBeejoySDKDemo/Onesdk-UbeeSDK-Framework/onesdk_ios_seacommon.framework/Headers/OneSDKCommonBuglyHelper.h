//
//  BuglyHelper.h
//  BaitianIosSdk
//
//  Created by tanjiatao01 on 16/12/12.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Bugly/Bugly.h>


//---------bugly---------
static NSString * const BUGLY_EXCEPTION_TITLE_LOGIN_TIMEOUT = @"LoginTimeout";
static NSString * const BUGLY_EXCEPTION_TITLE_LOGIN_WEBLOADFAIL = @"LoginWeb load fial";
static NSString * const BUGLY_EXCEPTION_TITLE_LOGIN_GETSESSIONFAIL = @"Login getSession fail";

static NSString * const BUGLY_EXCEPTION_TITLE_INIT_ONESDKFAIL = @"Onesdk init fail";
static NSString * const BUGLY_EXCEPTION_TITLE_INIT_YDSDKFAIL = @"Ydaccount init fail";

static NSString * const BUGLY_EXCEPTION_TITLE_PAY_GETOREDERID = @"Onesdk getOrderId fail";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_GETOREDERID_NULL = @"Onesdk getOrderId null";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_ORDERIDNULL=@"OrderId is Null";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_APPRECEIPT_UPLOAD_FAIL = @"Onesdk appreceipt upload server fail";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_PRODUCTID_WRONG = @"App ProductId is wrong";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_APPRECEIPT_INDB_UPLOAD_FAIL= @"Onesdk appreceipt in db upload server fail";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_NO_ABLE= @"No ablely to pay in app";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_GETTHEPRODUCE_TIMEOUT= @"Get the produce in app timeout";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_NOPRODUCT_INAPPSTORE= @"No productId in the appstore";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_PRODUCTID_NO_INAPPSTORE= @"ProductId is not in the appstore";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_NOAPPRECEIPT= @"appreceipt is null";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_FAILIN_APPSTORECALLBACK= @"Pay in the appstore Fail msg From Apple";
static NSString * const BUGLY_EXCEPTION_TITLE_PAY_FAILIN_NO_RECEIPECALLBACK =@"Pay Fail by appstore's Receipt is null ";


@interface OneSDKCommonBuglyHelper : NSException


+(void)initBugly;

+(void)reportExceptionWithReason:(NSString *)reason title:(NSString *)exceptionTitle url:(NSString *)requestUrl;

+(void)reportExceptionWithError:(NSError *)httperror;

+(void)reportExceptionWithReason:(NSString *)reason title:(NSString *)exceptionTitle;

+(void)reportExceptionWithIAP:(NSString *)orderId userId:(NSString *)userId roleId:(NSString *)roleId reasonMsg:(NSString *)reason;

+(void)reportExceptionWithResultIAP:(NSString *)orderId reasonMsg:(NSString *)reason;


@end
