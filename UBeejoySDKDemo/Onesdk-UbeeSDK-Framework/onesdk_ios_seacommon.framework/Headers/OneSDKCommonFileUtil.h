//
//  BtFileUtil.h
//  onesdk-ios-AsiaAreaSDK
//
//  Created by Tanjiatao on 2017/11/15.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OneSDKCommonFileUtil : NSObject

+ (void)getAppCachFile;

@end
