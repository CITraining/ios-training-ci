//
//  OneSDKHttpRequest.h
//  Taiyou_YuJun_OneSDK
//
//  Created by Tanjiatao on 2017/2/12.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>


static NSString  * const HTTP_RESULT_CODE = @"resultCode";
static NSString  * const HTTP_RESULT_CONTENT= @"resultContent";
static NSString  * const RequestType_POST = @"POST";
static NSString  * const RequestType_GET = @"GET";

/**
 * 网络连接报错   客户端错误码
 */
static NSString  * const NETWORK_ERROR = @"-100";
static NSString  * const NETWORK_DATA_NULL = @"-101";
static NSString  * const RESPONE_NULL = @"-102";
static NSString  * const REQUEST_ERROR = @"-103";
static NSString  * const URL_ILLEAGE = @"-104";//网址不合法
static NSString  * const DATA_ILLEAGE = @"-105";//post数据不合法


@interface OneSDKCommonHttpRequest : NSObject


/**
 * post请求 带数据
 **/
- (void)httpRequestAsync:(NSString *)url jsonString:(NSString *)jsonString
                 success:(void (^)(NSDictionary *dicinfo))successHandler
                 failure:(void (^)(NSDictionary *dicinfo))failHandler;

/**
 * post请求 带数据
 **/
- (void)httpRequestAsync:(NSString *)url jsonData:(NSData *)jsonData
                success:(void (^)(NSDictionary *dicinfo))successHandler
                failure:(void (^)(NSDictionary *dicinfo))failHandler;

/**
 * post请求 带数据 带header
 **/
- (void)httpRequestAsyncWithCustomHeader:(NSMutableDictionary *)header requestUrl:(NSString *)url jsonData:(NSData *)jsonData success:(void (^)(NSDictionary *))successHandler failure:(void (^)(NSDictionary *))failHandler;
/**
 * 普通的post请求 无数据
 **/
- (void)httpRequestAsync:(NSString *)url success:(void (^)(NSDictionary *))successHandler failure:(void (^)(NSDictionary *))failHandler;

@end
