//
//  OneSDKCommonNetWorkUtil.h
//  BtgameAppStore
//
//  Created by tanjiatao on 16/3/24.
//  Copyright © 2016年 tanjiatao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <Foundation/Foundation.h>

typedef enum {
    BTNotReachable = 0,
    BTReachableViaWiFi,
    BTReachableVia3G,
    BTReachableVia2G
} BTNetworkStatus;
#define kBTReachabilityChangedNotification @"kBTNetworkReachabilityChangedNotification"

@interface OneSDKCommonNetWorkUtil : NSObject{
    BOOL localWiFiRef;
    SCNetworkReachabilityRef reachabilityRef;
}
+ (NSString *)getCurrentNetStatus;

//reachabilityWithHostName- Use to check the reachability of a particular host name.
+ (OneSDKCommonNetWorkUtil*) reachabilityWithHostName: (NSString*) hostName;

//reachabilityWithAddress- Use to check the reachability of a particular IP address.
+ (OneSDKCommonNetWorkUtil*) reachabilityWithAddress: (const struct sockaddr_in*) hostAddress;

//reachabilityForInternetConnection- checks whether the default route is available.
//  Should be used by applications that do not connect to a particular host
+ (OneSDKCommonNetWorkUtil*) reachabilityForInternetConnection;

//reachabilityForLocalWiFi- checks whether a local wifi connection is available.
+ (OneSDKCommonNetWorkUtil*) reachabilityForLocalWiFi;

//Start listening for reachability notifications on the current run loop
- (BOOL) startNotifier;
- (void) stopNotifier;

- (BTNetworkStatus) currentReachabilityStatus;
//WWAN may be available, but not active until a connection has been established.
//WiFi may require a connection for VPN on Demand.
- (BOOL) connectionRequired;



@end
