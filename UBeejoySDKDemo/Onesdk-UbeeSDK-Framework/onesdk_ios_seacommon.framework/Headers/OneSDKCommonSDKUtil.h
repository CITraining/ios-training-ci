//
//  BtSDKUtil.h
//  onesdk-ios-AsiaAreaSDK
//
//  Created by Tanjiatao on 2017/11/15.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OneSDKCommonStringUtil.h"

#define KEY_IDFA  @"com.baitian.seasdk.idfa"
//uuid 保存在keychain中的key 少变动
#define KEY_UUID @"com.baitian.seasdk.uuid"
//屏幕相关
#define btKeyWindow [UIApplication sharedApplication].keyWindow
#define btDevice_Is_iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define btDevice_Is_iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define btDevice_Is_iPhone6 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define btDevice_Is_iPhone6Plus ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2208), [[UIScreen mainScreen] currentMode].size) : NO)
#define btDevice_Is_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#define btScreen_Bounds [UIScreen mainScreen].bounds
#define btScreen_Height [UIScreen mainScreen].bounds.size.height
#define btScreen_Width [UIScreen mainScreen].bounds.size.width
#define btNavigationbarHeigh [[UIApplication sharedApplication] statusBarFrame].size.height

#define viewScaleVertical  btScreen_Width/320 //以竖屏为基础进行设计布局
#define viewScaleLandscape ((isPad ? btScreen_Width/568 : btScreen_Height/320))//以横屏为基础进行设计布局
#define isLandScape (btScreen_Width>btScreen_Height)
#define btScaleForLandscape_iPhone5_Desgin(_X_) (isLandScape ? _X_ * viewScaleLandscape : _X_ * viewScaleVertical)
//页面基准比例 等比放大缩小
#define _btScaleSize_for_uiDesgin(_X_) (isLandScape ? _X_ * viewScaleLandscape : _X_ * viewScaleVertical)


//系统
#define IOS7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0)

//设备相关 是否全面屏幕
#define btDevice_Is_XProduct ([[[[UIApplication sharedApplication] delegate] window].safeAreaInsets.bottom] > 0.0 ? YES : NO)
#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

//版本号
#define _gameVersionName [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]
#define _gameVersionCode [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]
//SDK版本号
#define _sdkVersionCode [OneSDKCommonSDKUtil getValueFromBaitianPlist:@"IosSdkVersion" keyParentName:@"" keyParentType:String];
#define _gamepackageName [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]

//字符串相关
#define _formatString(s,...) [NSString stringWithFormat:(s), ##__VA_ARGS__]
#define _removeNewLineAndTrimming(s,...) [OneSDKCommonStringUtil removeNewlineStringAndTrimming:[NSString stringWithFormat:(s), ##__VA_ARGS__]]
#define _functionName [NSString stringWithFormat:@"%s(%d)",__FUNCTION__,__LINE__]
//Log相关
#ifdef DEBUG
    #define DebugLog(s, ...) NSLog(@"uBeejoyDebug %s(%d): %@", __FUNCTION__, __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__])
#else
    #define DebugLog(s, ...)
#endif
//信息log，需要打印，用与检测包体
#define InfoLog(s, ...) NSLog(@"uBeejoyInfo %s(%d): %@", __FUNCTION__, __LINE__, _removeNewLineAndTrimming(s,##__VA_ARGS__))





//线程相关
#define MAIN(block) dispatch_async(dispatch_get_main_queue(),block)
// 不堵塞线程并在主线程的延迟执行 timer:延迟时间，单位：秒；与主线程同步
#define GCDMainDelay(timer,block) dispatch_after(dispatch_time(DISPATCH_TIME_NOW, INT64_C(timer) * NSEC_PER_SEC), dispatch_get_main_queue(), block)
// 将code调到主线程执行，与主线程同步
#define GCDMain(block) dispatch_async(dispatch_get_main_queue(), block)




static NSString  * const SEASDK_PLIST_KEY = @"SeaSDKCommon";
static NSString  * const SEASDK_PLIST_RUNNING_TYPE_KEY= @"RunningType";
static NSString  * const SEASDK_PLIST_BUGLYENBALE_KEY= @"BuglyEnable";// 是否可用
static NSString  * const SEASDK_PLIST_BUGLYINITED_KEY= @"BuglyInited";// bugly是否需要sdk进行初始化
@interface OneSDKCommonSDKUtil : NSObject

typedef enum{
    Array,
    Dictionary,
    String,
    Int,
} PListValueType;

typedef enum{
    Local_Test=1,
    Online_Test,
    Release,
}RunningType;

+ (NSString *)getUUID;

+ (NSString *)getIdfa;

+ (void)setDefaultImeiInToKeyChain:(NSString *)defeaultImei;

+ (RunningType)getAppRunningType;

+(NSString*)deviceModelName;

/**
 * typeName  在String类型下可以为空,在array 与Dictionary中必须为其keyname
 */
+ (NSString *)getStringFromPlist:(NSString  *)key typeKeyName:(NSString *)typekeyName valueType:(PListValueType)plistValueType;

+ (int)getIntFromPlist:(NSString *)key typeKeyName:(NSString *)typeKeyName valueType:(PListValueType)plistValueType;

+ (NSString *)getValueFromBaitianPlist:(NSString *)key keyParentName:(NSString *)keyParentName keyParentType:(PListValueType)keyParentType;

+ (UIViewController *)appRootController;

+ (int) calcDaysFromBegin:(NSDate *)beginDate end:(NSDate *)endDate;

+ (int) calcSecondFromBegin:(NSDate *)beginDate end:(NSDate *)endDate;

+ (NSString *)timestampSwitchTime:(NSInteger)timestamp andFormatter:(NSString *)format;

+ (BOOL)isIphoneX;

+ (void)cleanAppCaches;
@end
