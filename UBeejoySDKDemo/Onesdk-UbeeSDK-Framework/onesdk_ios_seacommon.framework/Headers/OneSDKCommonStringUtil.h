//
//  BtStringUtil.h
//  onesdk-ios-AsiaAreaSDK
//
//  Created by Tanjiatao on 2017/11/15.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GTMBase64.h"
@interface OneSDKCommonStringUtil : NSObject

+ (BOOL)isBlankString:(NSString *)string;

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

+ (NSString *) encodewithBase64:(NSString *)sourceStr;

+ (NSString *) decodewithBase64:(NSString *)encodeStr;

+(NSString  *)dicToJson:(NSDictionary *)dic;

+(NSString *)removeNewlineStringAndTrimming:(NSString *)str;

@end
