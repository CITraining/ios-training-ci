//
//  OneSDKLogger.h
//  onesdk-ios-seacommon
//
//  Created by Tanjiatao on 2018/10/16.
//  Copyright © 2018年 zengzhifeng. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OneSDKLogger : NSObject

/**
 *  调试log，上线时，需要屏蔽掉
 *
 *  @param logObject 需要输出的日志，如果为字典，会转为单行的字符串。
 *  @param functionName 调用的方法名称
 *
 */
+(void)debug:(NSObject *)logObject func:(NSString *)functionName;


+(void)debug:(NSString *)functionName withLogString:(NSString *)logFormat,...;

@end

NS_ASSUME_NONNULL_END
