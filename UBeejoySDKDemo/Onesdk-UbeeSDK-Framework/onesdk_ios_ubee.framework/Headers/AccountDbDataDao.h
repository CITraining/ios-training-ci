//
//  AccountDataDao.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2018/10/30.
//  Copyright © 2018年 zengzhifeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountDbData.h"

@interface AccountDbDataDao : NSObject

+ (AccountDbDataDao *)shareSDKInstance;

-(void)initDB;
//更新全部信息，主要在账号登录时
-(BOOL)updateAccount:(AccountDbData *)account;

//更新密码
-(BOOL)updatePasswordByUserId:(NSString *)userId password:(NSString *)pwd;



//获取所有的账号记录
-(NSMutableArray *)getAccountList;

//获取最新的账号记录
-(NSMutableArray *)getLastTimeUpdateAccount;

//获取密码
-(NSString *)getPasswordByUserId:(NSString *)userId;

-(BOOL)deleteAccountInfoByUserId:(NSString *)userID;

-(BOOL)deleteAccountInfoByAccountName:(NSString *)accountName;



@end

