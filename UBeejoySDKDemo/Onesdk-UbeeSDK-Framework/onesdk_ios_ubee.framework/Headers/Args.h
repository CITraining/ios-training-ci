//
//  Args.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/5.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Args : NSObject

@property(nonatomic, strong)NSString *arg1;
@property(nonatomic, strong)NSString *arg2;
@property(nonatomic, strong)NSString *arg3;
@property(nonatomic, strong)NSString *arg4;
@property(nonatomic, strong)NSString *arg5;
@property(nonatomic, strong)NSString *arg6;
@property(nonatomic, strong)NSString *arg7;
@property(nonatomic, strong)NSString *arg8;
@property(nonatomic, strong)NSString *arg9;
@property(nonatomic, strong)NSString *arg10;

@end
