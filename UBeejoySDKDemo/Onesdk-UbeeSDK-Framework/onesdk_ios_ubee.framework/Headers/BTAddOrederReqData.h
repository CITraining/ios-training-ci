//
//  BTAddOrederReqData.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/26.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Device.h"
#import <onesdk_ios_seacommon/NSObject+YYModel.h>


static int const IOS_PAYCHANNEL = 500;
@interface BTAddOrederReqData : NSObject

@property(nonatomic,strong)Device *device;
@property(nonatomic,strong)NSString *appId;
@property(nonatomic,assign)int packageId;
@property(nonatomic,strong)NSString *userId;
@property(nonatomic,strong)NSString *serverId;
@property(nonatomic,strong)NSString *serverName;
@property(nonatomic,strong)NSString *roleId;
@property(nonatomic,strong)NSString *roleName;
@property(nonatomic,strong)NSString *productCode;//品项代号 需要在后台配置
@property(nonatomic,assign)int payChannelId;
@property(nonatomic,strong)NSString *callBackInfo;
@property(nonatomic,strong)NSString *notifyUrl;
@property(nonatomic,strong)NSString *gameOrderId;//游戏方的订单ID
@property(nonatomic,strong)NSString *sign;

@end
