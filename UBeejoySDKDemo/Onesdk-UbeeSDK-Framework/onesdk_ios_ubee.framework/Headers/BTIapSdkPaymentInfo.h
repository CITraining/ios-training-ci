//
//  BTIapSdkPaymentInfo.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/24.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BTIapSdkPaymentInfo : NSObject

@property(nonatomic, strong)NSString *userId;//用户Id
@property(nonatomic, strong)NSString *roleId;//角色Id
@property(nonatomic, strong)NSString *roleName;//角色名称
@property(nonatomic, strong)NSString *roleLevel;//角色等级
@property(nonatomic, strong)NSString *serverId;//服务器Id
@property(nonatomic, strong)NSString *serverName;//服务器名称
@property(nonatomic, strong)NSString *money;//付费金额
@property(nonatomic, strong)NSString *callBackInfo;//透传字段
@property(nonatomic, strong)NSString *goodsDesc;//商品描述
@property(nonatomic, strong)NSString *goodsId;
@property(nonatomic, strong)NSString *productCode;
@property(nonatomic, strong)NSString *outOrderNo;
@property(nonatomic, strong)NSString *filterParam;
@property(nonatomic, strong)NSString *notifyUrl;
@end
