//
//  BTServerOrderInfo.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/26.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UBeeApiResult.h"
#import "NSObject+YYModel.h"
#import "Args.h"
@interface BTServerOrderInfo : NSObject
@property(nonatomic, strong)NSString *orderId;
@property(nonatomic, strong)NSString *payChannelId;
@property(nonatomic, strong)NSString *redirectUrl;
@property(nonatomic, strong)Args *extArgs;

@end
