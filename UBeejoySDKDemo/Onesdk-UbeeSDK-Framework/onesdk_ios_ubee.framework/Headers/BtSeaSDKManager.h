//
//  BtSeaSDKManager.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/5.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "UBeeAccountData.h"
#import "UBeeApiResult.h"
#import "UBeeAccountInfo.h"
#import "UBeeAccountManager.h"
#import "UBeeInfoSettingUtil.h"
#import "BTIapSdkPaymentInfo.h"
#import "SeaSdkPayManager.h"
#import "BTRoleInfo.h"


/**
 @protocol
 A delegate for `BtSeaSDKManager`
 */
@protocol BtSeaSDKManagerDelegate <NSObject>

@required
- (void)initResult:(UBeeApiResult *)apiResult;
- (void)loginResult:(UBeeApiResult *)result userInfo:(UBeeAccountInfo *)userInfo;
- (void)logoutResult:(UBeeApiResult *)result;
- (void)payResult:(UBeeApiResult *)apiResult;
@end

@interface BtSeaSDKManager : NSObject
@property(nonatomic,strong)id<BtSeaSDKManagerDelegate> sdkdelegate;


+ (BtSeaSDKManager *_Nullable)shareSDKInstance;

- (void)application:(UIApplication *_Nullable)application didFinishLaunchingWithOptions:(NSDictionary *_Nullable)launchOptions;

- (BOOL)application:(UIApplication *_Nullable)app openURL:(NSURL *_Nullable)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *_Nullable)options;

- (BOOL)application:(UIApplication *_Nullable)application openURL:(NSURL *_Nullable)url sourceApplication:(NSString *_Nullable)sourceApplication annotation:(id _Nullable )annotation;

- (void)applicationDidBecomeActive:(UIApplication *_Nullable)application ;

- (void)application:(UIApplication*_Nullable)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData*_Nullable)deviceToken;

- (BOOL) application:(UIApplication *_Nullable)application
continueUserActivity:(NSUserActivity *_Nullable)userActivity restorationHandler:(void
                                                                        (^_Nullable)(NSArray *_Nullable))restorationHandler;

-(void)showUserCenter;

-(void)enterGame:(BTRoleInfo *)btRoleInfo;

-(void)upgradeRole:(BTRoleInfo *)btRoleInfo;

-(void)createRole:(BTRoleInfo *)btRoleInfo;

//应用邀请
- (void)inviteFriendsToOpenTheAppLink:(NSString *)appDeepLinkUrl appimgurl:(NSString *)appImgUrl
                             success:(void (^)(NSDictionary *dicinfo))successHandler
                             failure:(void (^)(NSDictionary *dicinfo))failHandler;

-(void)sharewithImgs:(NSArray *)imgs
         contenttext:(NSString *)text
               title:(NSString *)title
                 url:(NSString *)url
                type:(UBee_ShareType)shareType
             success:(void (^)(UBeeApiResult *result ))successHandler
             failure:(void (^)(UBeeApiResult *result ))failHandler;

- (void)initSdk;
- (void)login;
- (void)logout;
- (void)payWith:(BTIapSdkPaymentInfo *)paymentInfo args:(Args *)args;

- (void)logoutResult:(UBeeApiResult *)result;
- (void)loginResult:(UBeeApiResult *)result userInfo:(UBeeAccountInfo *)userInfo;

-(void)rateUS;
- (void) onFinishNewRoleTutorial;
- (void) onObtainNewRolePack;
- (void) reportShareResult:(NSString *)shareType shareDes:(NSString *)shareDes shareResult:(int)shareRes;


@end


