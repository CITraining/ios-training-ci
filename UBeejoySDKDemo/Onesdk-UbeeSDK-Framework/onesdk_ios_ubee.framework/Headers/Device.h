//
//  Device.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/4.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Device : NSObject


@property(nonatomic, strong)NSString *sdkVersion;//sdk版本
@property(nonatomic, strong)NSString *phoneModel;//手机型号
@property(nonatomic, assign)int sdkType;
@property(nonatomic, strong)NSString *imei;
@property(nonatomic, strong)NSString *imsi;
@property(nonatomic, strong)NSString *gameVersionName;
@property(nonatomic, strong)NSString *gameVersionCode;
@property(nonatomic, strong)NSString *gamepackageName;
@property(nonatomic, assign)int density;
@property(nonatomic, assign)int displayScreenWidth;
@property(nonatomic, assign)int displayScreenHeight;
@property(nonatomic, strong)NSString *mac;
@property(nonatomic, strong)NSString *networkInfo;
@property(nonatomic, strong)NSString *miniChannelId;
@property(nonatomic, strong)NSString *sysVersion;
@property(nonatomic, strong)NSString *packageId;
@property(nonatomic, strong)NSString *lang;
@property(nonatomic, strong)NSString *adjustAdId;
+ (Device *)shareSDKInstance;

@end
