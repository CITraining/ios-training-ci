//
//  ForumObject.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2018/4/27.
//  Copyright © 2018年 zengzhifeng. All rights reserved.
//

#import <OnesdkBaitianFramework/OnesdkBaitianFramework.h>
#import <Foundation/Foundation.h>

@interface ForumObject : NSObject

typedef enum{
    OPEN_HOMEPAGE = 1,
    SCREEN_SHOT =2,
    VIDEO_STARTRECORD=3,
    VIDEO_STOPRECORD=4,
    SHOW_WIDGET=5,
}ForumOpType;

typedef enum{
    CAFE = 1,
}ForumPlatformType;


@property(nonatomic, assign)ForumOpType forumOpType;
@property(nonatomic, assign)ForumPlatformType forumPlatformType;
@property(nonatomic, strong)NSString *imageUri;
@property(nonatomic, strong)NSString *videoUri;


@end
