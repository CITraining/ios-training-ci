//
//  SeaSdKPayApi.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/24.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BTIapSdkPaymentInfo.h"
#import "Args.h"
#import "BTServerOrderInfo.h"
typedef enum{
    Add_Order = 1,
    Verify_Order,
}API_Type;

@interface SeaSdKPayApi : NSObject

+ (SeaSdKPayApi *)shareSDKInstance;

//下单获取orderId
-(void)addOrderWithAppId:(NSString *)appId chargeData:(BTIapSdkPaymentInfo*)paymentInfo args:(Args*)args
                 success:(void(^)(BTServerOrderInfo *OrderRespData))successBlock
                    fail:(void(^)(UBeeApiResult *result))failBlock;

- (NSString *)getRequestUrlwithApiType:(API_Type)apiType;

@end
