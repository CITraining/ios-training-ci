//
//  SeaSdkLoginApi.h
//  onesdk-ios-seaLogin
//
//  Created by Tanjiatao on 2017/12/4.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Args.h"
#import "UBeeAccountInfo.h"
#import "UBeeApiResult.h"
#import "UBeeErrorCode.h"
#import "ServerSettingInfo.h"


/**
 仅提供API 不提供其他操作
 */
@interface SeaSdkAccountApi : NSObject

+ (SeaSdkAccountApi *)shareSDKInstance;

#pragma mark 获取配置信息
- (void)uBeeServerSettingCenterSuccess:(void (^)(ServerSettingInfo *ubUserInfo))successHandler
                        failure:(void (^)(UBeeApiResult *reslut))failHandler;

#pragma mark 账号登录
- (void)uBeeAccountLogin:(NSString *)account password:(NSString *)pwd success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
                 failure:(void (^)(UBeeApiResult *reslut))failHandler;
//包括账号与游客的自动登录
- (void)uBeeAccountAutoLogin:(NSString *)account identity:(NSString *)identity accountType:(AccountType)accountType
                     success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
                     failure:(void (^)(UBeeApiResult *reslut))failHandler;
//游客登陆
- (void)uBeeVisitorLoginWithBlock:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
                         failure:(void (^)(UBeeApiResult *reslut))failHandler;

#pragma mark 账号注册
//普通注册
- (void)uBeeAccountRegister:(NSString *)account password:(NSString *)pwd
                    success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
                    failure:(void (^)(UBeeApiResult *reslut))failHandler;

//identity 从游客升级的话，不为空 游客绑定官网账号
- (void)uBeeVisitorRegisterToAccount:(NSString *)account password:(NSString *)pwd identity:(NSString *)identity
                            success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
                            failure:(void (^)(UBeeApiResult *reslut))failHandler;

#pragma mark 账号密码处理
- (void)uBeeChanngePWD:(NSString *)account oldpassword:(NSString *)pwd newpassword:(NSString *)npwd
               success:(void (^)(void))successHandler
               failure:(void (^)(UBeeApiResult *reslut))failHandler;


/**
 重置密码时，请求邮箱验证码，与绑定邮箱时的验证码接口不一样

 @param account 忘记密码的账号
 @param successHandler 升级成功的回调
 @param failHandler 失败的回调
 */
-(void)getEmailcodeforResetPwd:(NSString *)account
                       success:(void (^)(void))successHandler
                       failure:(void (^)(UBeeApiResult *reslut))failHandler;

-(void)reSetPwdByEmail:(NSString *)account emailCode:(NSString *)verifyCode newPassword:(NSString *)newPwd
                    success:(void (^)(void))successHandler
                    failure:(void (^)(UBeeApiResult *reslut))failHandler;


#pragma mark Email业务
//- (void)uBeeFindPwdByEmail:(NSString *)account email:(NSString *)email
//                   success:(void (^)(void))successHandler
//                   failure:(void (^)(UBeeApiResult *reslut))failHandler DEPRECATED_MSG_ATTRIBUTE("After 2.0 version,this api was Deprecated");;

- (void)uBeeBindGetEmailCode:(NSString *)account password:(NSString *)pwd email:(NSString *)email
                     success:(void (^)(void))successHandler
                     failure:(void (^)(UBeeApiResult *reslut))failHandler;

- (void)uBeeEmailCodeCheck:(NSString *)account password:(NSString *)pwd email:(NSString *)email emailCode:(NSString *) code
                   success:(void (^)(void))successHandler
                   failure:(void (^)(UBeeApiResult *reslut))failHandler ;

- (void)uBeeBindGetEmailCodeV2:(NSString *)account identity:(NSString *)identity email:(NSString *)email
                     success:(void (^)(void))successHandler
                     failure:(void (^)(UBeeApiResult *reslut))failHandler;

- (void)uBeeEmailCodeCheckV2:(NSString *)account identity:(NSString *)identity email:(NSString *)email emailCode:(NSString *) code
                     success:(void (^)(void))successHandler
                     failure:(void (^)(UBeeApiResult *reslut))failHandler;




/**
 检查用户是否绑定过邮箱

 @param account 用户账号名称
 @param successHandler 升级成功的回调
 @param failHandler 失败的回调
 */
- (void)emailcheckBind:(NSString *)account
               success:(void (^)(NSDictionary *dicinfo))successHandler
               failure:(void (^)(UBeeApiResult *reslut))failHandler;

#pragma mark 第三方账户登录业务

/**
 第三方账号进行登录

 @param thirdToken 第三方的Token
 @param args 额外参数
 @param thirdId 平台ID。 Facebook为1
 @param thirdUserId 第三方平台的用户ID
 @param successHandler 升级成功的回调
 @param failHandler 失败的回调
 */
- (void)uBeeThirdPartLogin:(NSString *)thirdToken extArgs:(Args *)args thirdPlatfromId:(int)thirdId thirdUserId:(NSString *) thirdUserId success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler failure:(void (^)(UBeeApiResult *reslut))failHandler;


/**
 第三方账号升级为UBee账号

 @param userAccount 用户输入的UBee账号名称
 @param userPwd 用户输入的UBee账号密码
 @param currentIdentity 当前第三方登录的Identity
 @param successHandler 升级成功的回调
 @param failHandler 失败的回调
 */
- (void)thirdPartAccountUpgradToUBAccount:(NSString *)userAccount password:(NSString *)userPwd currentIdentity:(NSString *)currentIdentity thirdIdType:(int)thirdIdType success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
                                  failure:(void (^)(UBeeApiResult *reslut))failHandler;


- (void)thirdPartAccountCheckBind:(NSString *)userID
                                  success:(void (^)(NSDictionary *dicinfo))successHandler
                                  failure:(void (^)(UBeeApiResult *reslut))failHandler;


#pragma mark 升级至第三方账户业务   游客绑定FB GP
/*
 *游客升级成FB账号或GP账号
 *2.0版本之后 该API停用
 */
- (void)uBeeThirdPartUpgrad:(NSString *)thirdToken extArgs:(Args *)args thirdPlatfromId:(int)thirdIdType thirdUserId:(NSString *) thirdUserId visitorUser:(UBeeAccountInfo *)visitor success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
                    failure:(void (^)(UBeeApiResult *reslut))failHandler DEPRECATED_MSG_ATTRIBUTE("After 2.0 version,this api was Deprecated");

@end
