//
//  SeaSdkPayManager.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/26.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UBeeApiResult.h"
#import "BTServerOrderInfo.h"
#import "BTIapSdkPaymentInfo.h"
@protocol SeaSdkPayManagerDelegate <NSObject>

@required
- (void)payResult:(UBeeApiResult *)apiResult;


@end

@interface SeaSdkPayManager : NSObject

@property(nonatomic,strong)id<SeaSdkPayManagerDelegate> paydelegate;

+ (SeaSdkPayManager *)shareSDKInstance;

- (void)initSdkPayManager;

- (void)payWith:(BTIapSdkPaymentInfo *)paymentInfo args:(Args*)args;


@end
