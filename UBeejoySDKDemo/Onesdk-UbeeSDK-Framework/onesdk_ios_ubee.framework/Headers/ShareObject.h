//
//  ShareObject.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2018/4/24.
//  Copyright © 2018年 zengzhifeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <onesdk_ios_seacommon/NSObject+YYModel.h>
@interface ShareObject : NSObject

typedef enum{
    UBee_SharePlatForm_FB = 1,
    UBee_SharePlatForm_Kakao =2
}UBee_SharePlatform;

@property(nonatomic, strong)NSString *shareUrl;
@property(nonatomic, strong)NSString *title;
@property(nonatomic, strong)NSString *des;
@property(nonatomic, strong)NSString *imgUrl;
@property(nonatomic, strong)NSString *imgPath;
@property(nonatomic, strong)NSString *templateId;
@property(nonatomic, strong)NSString *downloadText;
@property(nonatomic, strong)NSMutableDictionary *extra;
@property(nonatomic, assign)int sharePlatform;



@end
