//
//  SocialManager.h
//  onesdk-ios-socialmoudle
//
//  Created by Tanjiatao on 2018/5/8.
//  Copyright © 2018年 zengzhifeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShareObject.h"
#import "ForumObject.h"
#import "UBeeApiResult.h"

@protocol SocialShareDelegate <NSObject>
@required
- (void)onSocialShareSuccess:(NSString *)platform;
- (void)onSocialShareCancel:(NSString *)platform;
- (void)onSocialShareError:(NSString *)platform;
@end

@protocol SocialFromDelegate <NSObject>
@required
- (void)onSocialFromInit:(BOOL)result;
- (void)onSocialFromShowForum;
- (void)onSocialFromHideForum;
@end


@interface SocialManager : NSObject
@property(nonatomic,strong)id<SocialShareDelegate> shareDelegate;
@property(nonatomic,strong)id<SocialFromDelegate> forumDelegate;


+ (SocialManager *_Nullable)shareSDKInstance;

- (BOOL)application:(UIApplication *_Nullable)application openURL:(NSURL *_Nullable)url sourceApplication:(NSString *_Nullable)sourceApplication annotation:(id _Nullable )annotation;


- (void)initForum;
- (void)share:(ShareObject *)shareObject;

- (void)forumStartWidget;
- (void)forum;
- (void)forumScreentShot;
- (void)forumStartScreentRecord;
- (void)forumStopScreentRecord;

/*SDK的账号发生变化*/
- (void)sdkLogout;
- (void)syncGameUserId:(NSString *)userId;

@end
