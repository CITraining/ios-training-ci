//
//  UBeeAccountData.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/4.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import "UBeeJoyBasePostData.h"

@interface UBeeAccountData : UBeeJoyBasePostData

@property(nonatomic, strong)NSString *account;
@property(nonatomic, strong)NSString *pwd;
@property(nonatomic, strong)NSString *identity;//登录成功的加密串
@property(nonatomic, strong)NSString *oPw;
@property(nonatomic, strong)NSString *nPw;

- (instancetype)initWithAccount:(NSString *)account andPwd:(NSString *)pwd;

- (instancetype)initWithAccount:(NSString *)account andPwd:(NSString *)pwd identity:(NSString *)identity;

@end
