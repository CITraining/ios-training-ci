//
//  UBeeAccountEmailData.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/5.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import "UBeeAccountData.h"

@interface UBeeAccountEmailData : UBeeAccountData

@property(nonatomic, strong)NSString *code;
@property(nonatomic, strong)NSString *email;


@end
