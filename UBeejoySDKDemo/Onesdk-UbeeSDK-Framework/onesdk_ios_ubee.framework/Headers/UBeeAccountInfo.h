//
//  UBeeAccountInfo.h
//  onesdk-ios-ubee
//  服务器返回的登录结果
//  Created by Tanjiatao on 2017/12/5.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import "UBeeJoyBasePostData.h"
#import "UBeeResponseData.h"
#import "NSObject+ToString.h"
@interface UBeeAccountInfo : UBeeResponseData

typedef enum{
    FaceBook=1,
    Google =2,
    UBee = 3,
    UBeeVisitor = 4,
    Kakao = 5
}AccountType;

@property(nonatomic, strong)NSString *account;
@property(nonatomic, strong)NSString *token;
@property(nonatomic, strong)NSString *identity;//用于下次自动登录 需要保存起来
@property(nonatomic, strong)NSString *userId;
@property(nonatomic, assign)BOOL visitor;
@property(nonatomic, assign)BOOL needTip;
@property(nonatomic, assign)int accountType;
@property(nonatomic, assign)BOOL firstJoin;
@property(nonatomic, strong)NSString *firstJoinDate;
@end
