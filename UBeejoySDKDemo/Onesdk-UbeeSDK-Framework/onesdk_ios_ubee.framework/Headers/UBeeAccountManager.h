//
//  UBeeAccountManager.h
//  onesdk-ios-ubee
//  管理账户相关信息
//  Created by Tanjiatao on 2017/12/7.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UBeeAccountInfo.h"
#import "UBeeAccountData.h"
#import "ServerSettingInfo.h"
@protocol UBeeAccountManagerDelegate <NSObject>

@required
- (void)initResult:(UBeeApiResult *)apiResult;
- (void)loginResult:(UBeeApiResult *)result userInfo:(UBeeAccountInfo *)userInfo;
- (void)logoutResult:(UBeeApiResult *)result;


@end
static NSString * const UBEE_LOGINCHANNEL_FACEBOOK = @"FACEBOOK";
static NSString * const UBEE_LOGINCHANNEL_GOOGLE = @"GOOGLE";
static NSString * const UBEE_LOGINCHANNEL_VISITOR = @"VISITOR";
static NSString * const UBEE_LOGINCHANNEL_U_BEE_JOY = @"U_BEE_JOY";
static NSString * const UBEE_LOGINCHANNEL_KAKAO = @"KAKAO";
@interface UBeeAccountManager : NSObject
@property(nonatomic,strong)id<UBeeAccountManagerDelegate> accountdelegate;
@property(nonatomic,assign)BOOL isBindAccount;

+ (UBeeAccountManager *)shareSDKInstance;

#pragma mark 账号管理
/**
获取服务器上的设置
 @return 设置类
 */
- (ServerSettingInfo *)getServerSettingInfo;


/**
 获取当前登录的用户信息
 @return 用户信息类
 */
- (UBeeAccountInfo *)getCurrentAccountInfo;


/**
保存用户的账号密码到数据库中
 @param userId 用户ID
 @param password 用户密码
 */
- (void)saveUserIDPassword:(NSString *)userId password:(NSString *)password;


/**
 获取的用的密码
 
 @param userId 用户ID
 @return 字符串的密码，MD5后的值
 */
- (NSString *)getPasswordByUserId:(NSString *)userId;

/**
保存最近登录的用户信息
 @param accountInfo 用户信息
 */
- (void)saveLastTimeLoginAccountIntoCache:(UBeeAccountInfo *)accountInfo;
/**
 获取最近登录的用户信息
 @return accountInfo 用户信息
 */
- (UBeeAccountInfo *)getLastTimeLoginAccountFromCache;


/**
 获取游客绑定的UB账号信息，用于playnow时使用

 @param accountInfo <#accountInfo description#>
 */
- (void)saveVisitorBindAccoutInToCache:(UBeeAccountInfo *)accountInfo;
-(UBeeAccountInfo *)getVisitorBindAccoutInfoFromCache;


/**
 保存游客登录信息，用于playnow时使用

 @param accountInfo <#accountInfo description#>
 */
- (void)saveVisitorAccountInToCache:(UBeeAccountInfo *)accountInfo;
-(UBeeAccountInfo *)getVisitorAccountInfoFromCache;


/**
 更新当前的用户信息

 @param accountInfo <#accountInfo description#>
 */
- (void)updateCurrentUBeeAccount:(UBeeAccountInfo *)accountInfo;

/**
保存RememberMe的账号名
 @param accountName
 */
- (void)rememberUBeeAccountName:(NSString *)accountName;
- (NSString *)getRememberUBeeAccountName;

- (void)changePwdInUserCerter;
- (BOOL)getChangePwdInUserCerterFlag;

- (void)setAutoLoginToDefaults:(BOOL)isAutoLogin;
- (BOOL)isAutoLoginByDefaults;
- (AccountType)getLastTimeLoginAccountType;






#pragma mark 业务方法
- (void)initSDK;
- (void)login;
- (void)logoutsuccess;
- (void)notifyloginResult:(UBeeApiResult *)result userInfo:(UBeeAccountInfo *)userInfo;
- (void)hideLoadingView;
- (void)showLoadingView:(NSString *)msg;
- (void)showErrorMsg:(NSString *)msg;
@end
