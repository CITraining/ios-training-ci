//
//  UBeeServerResult.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/5.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
static int SUCCESSCODE = 0;
static NSString * const SUCCESSMSG = @"success";
@interface UBeeApiResult : NSObject



@property(nonatomic, assign)int code;
@property(nonatomic, strong)NSString *msg;


-(instancetype)initWithSuccesapiResult;

@end
