//
//  UBeeFbSDKManager.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/6.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "UBeeAccountInfo.h"
#import "UBeeApiResult.h"
#import "UBeeInfoSettingUtil.h"

@interface UBeeFbSDKManager : NSObject<FBSDKSharingDelegate,FBSDKAppInviteDialogDelegate>

+ (UBeeFbSDKManager *_Nonnull)shareSDKInstance;

- (NSString *)getCurrentFBUserId;
- (void)FBLoginInView:(UIViewController *)viewController success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
        failure:(void (^)(UBeeApiResult *reslut))failHandler;

- (void)logoutsuccess:(void (^)(void))successHandler
                            failure:(void (^)(UBeeApiResult *reslut))failHandler;


- (void)visitorBindToFb:(UIViewController *_Nonnull)viewController visitorUserInfo:(UBeeAccountInfo *_Nonnull)visitor
               success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
               failure:(void (^)(UBeeApiResult *reslut))failHandler
                DEPRECATED_MSG_ATTRIBUTE("After 2.0 version,this api was Deprecated");

- (BOOL)isInstalledFacebookApp;

- (void)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

- (void)applicationDidBecomeActive:(UIApplication *)application ;

- (void)application:(UIApplication*)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;

- (BOOL) application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void
                                                                        (^_Nonnull)(NSArray *_Nullable))restorationHandler;


- (void)sharewithImgs:(NSArray *)imgs contenttext:(NSString *)text title:(NSString *)title url:(NSString *)url type:(UBee_ShareType)shareType success:(void (^)(UBeeApiResult *result ))successHandler failure:(void (^)(UBeeApiResult *result))failHandler;

-(void)inviteFriendsToOpenTheAppLink:(NSString *)appDeepLinkUrl appimgurl:(NSString *)appImgUrl
                             success:(void (^)(UBeeApiResult *))successHandler failure:(void (^)(UBeeApiResult *))failHandler;

- (void)eventTrack:(NSString *)eventName parameters:(NSDictionary *)dictionary;
- (void)eventTrackPurchase:(NSString *)eventName amount:(double)money parameters:(NSDictionary *)dictionary;



@end
