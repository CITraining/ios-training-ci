//
//  UBeeGoogleSDKManager.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/7.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UBeeAccountInfo.h"
#import "UBeeApiResult.h"
#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
@interface UBeeGoogleSDKManager : NSObject<GIDSignInDelegate,GIDSignInUIDelegate>

+ (UBeeGoogleSDKManager *)shareSDKInstance;


- (NSString *)getCurrentGPUserId;

- (void)GPLogin:(UIViewController *)viewController success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
        failure:(void (^)(UBeeApiResult *reslut))failHandler;

- (void)visitorToGp:(UIViewController *)viewController visitorUserInfo:(UBeeAccountInfo *)visitor
            success:(void (^)(UBeeAccountInfo *ubUserInfo))successHandler
            failure:(void (^)(UBeeApiResult *reslut))failHandler  DEPRECATED_MSG_ATTRIBUTE("After 2.0 version,this api was Deprecated");


- (void)logoutsuccess:(void (^)(void))successHandler
              failure:(void (^)(UBeeApiResult *reslut))failHandler;

- (void)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation;

- (void)applicationDidBecomeActive:(UIApplication *)application ;

- (void)application:(UIApplication*)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken;

- (BOOL) application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void
                                                                        (^_Nonnull)(NSArray *_Nullable))restorationHandler;
                                                                        

@end
