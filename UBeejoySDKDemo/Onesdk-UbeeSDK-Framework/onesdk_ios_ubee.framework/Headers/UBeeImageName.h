//
//  UBeeImageName_h.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/13.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
static NSString * const LOGO_ubee  = @"ublogo.png" ;

static NSString * const ICON_account  = @"icon_account.png" ;
static NSString * const ICON_password  = @"icon_pwd.png" ;
static NSString * const ICON_oldpassword  = @"icon_oldpwd.png" ;
static NSString * const ICON_ubee  = @"icon_ub.png" ;
static NSString * const ICON_fb  = @"icon_facebook.png" ;
static NSString * const ICON_gp  = @"icon_google.png" ;
static NSString * const ICON_kakao  = @"icon_kakao.png" ;
static NSString * const ICON_code  = @"icon_code.png" ;
static NSString * const ICON_mail  = @"icon_mail.png" ;
static NSString * const ICON_password_see  = @"icon_pwd_see.png" ;
static NSString * const ICON_password_unsee  = @"icon_pwd_unsee.png" ;

static NSString * const BUTTON_DISABLE = @"btn_disable.png";

static NSString * const BUTTON_GRAY_NORMAL = @"btn_gray_normal.png";
static NSString * const BUTTON_LIGHTGRAY_PRESS = @"btn_gray_press.png";

static NSString * const BUTTON_WIRHT_NORMAL = @"btn_withe_normal.png";
static NSString * const BUTTON_WIRHT_PRESS = @"btn_withe_press.png";

static NSString * const BUTTON_YELLOW_NORMAL = @"btn_yellow_normal.png";
static NSString * const BUTTON_YELLOW_PRESS = @"btn_yellow_press.png";

static NSString * const BUTTON_BACK_NORMAL = @"btn_back_normal.png";
static NSString * const BUTTON_BACK_PRESS = @"btn_back_press.png";

static NSString * const CHECKBOX_CHECKED = @"btn_check_on.png";
static NSString * const CHECKBOX_UNCHECKED = @"btn_check_off.png";


static NSString * const PROMISSION_ICON_CAMERA = @"icon_camera.png";
static NSString * const PROMISSION_ICON_MIC = @"icon_mic.png";
static NSString * const PROMISSION_ICON_PIC = @"icon_pic.png";
static NSString * const PROMISSION_ICON_FILESAVE = @"icon_save.png";
static NSString * const PROMISSION_BUTTON_NORMAL = @"btn_permission_normal.png";
static NSString * const PROMISSION_BUTTON_PRESS = @"btn_permission_pressed.png";


static NSString * const ICON_Bind_Email_Tips  = @"icon_tipslogo.png" ;

@interface UBeeImageName: NSObject

@end
