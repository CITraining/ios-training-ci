//
//  UBeeInfoSettingUtil.h
//  onesdk-ios-ubee
//  用于获取bundle中设置的值
//  Created by Tanjiatao on 2017/12/4.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import "UBeeJoyBasePostData.h"
/*全局通用*/
#define logoName NSLocalizedString(@"logoName",nil)
#define text_unknown NSLocalizedString(@"text_unknown",nil)
#define goToStore_message NSLocalizedString(@"tips_rateus",nil)
#define google_login_resultMsg NSLocalizedString(@"google_login_resultMsg",nil)
#define facebook_login_cancle NSLocalizedString(@"facebook_login_cancle",nil)
#define facebooke_share_appnotfindTips NSLocalizedString(@"facebooke_share_appnotfindTips",nil)
#define default_comfirm_text NSLocalizedString(@"default_comfirm_text",nil)
#define default_back_text NSLocalizedString(@"default_back_text",nil)

/*网络问题提示*/
#define no_network_tips NSLocalizedString(@"no_network_tips",nil)
#define web_reload NSLocalizedString(@"web_reload",nil)
#define web_reload_title NSLocalizedString(@"web_reload_title",nil)



/*权限页*/
#define permission_tv_title NSLocalizedString(@"permission_tv_title",nil)
#define permission_deny_tv_title NSLocalizedString(@"permission_deny_tv_title" ,nil)
#define permission_deny_tv_des  NSLocalizedString(@"permission_deny_tv_des",nil)
#define permission_mic_title NSLocalizedString(@"permission_mic_title" ,nil)
#define permission_mic_des NSLocalizedString(@"permission_mic_des" ,nil)
#define permission_camera_title NSLocalizedString(@"permission_camera_title" ,nil)
#define permission_camera_des NSLocalizedString(@"permission_camera_des" ,nil)
#define permission_filesave_title NSLocalizedString(@"permission_filesave_title" ,nil)
#define permission_filesave_des NSLocalizedString(@"permission_filesave_des" ,nil)
#define permission_pic_title NSLocalizedString(@"permission_pic_title" ,nil)
#define permission_pic_des NSLocalizedString(@"permission_pic_des",nil)
#define permission_button_ComfirmText NSLocalizedString(@"permission_button_ComfirmText",nil)


/*自动登录*/
#define welcome_tips NSLocalizedString(@"welcome_tips",nil)
#define autologin_countdown_tips NSLocalizedString(@"autologin_countdown_tips",nil)
#define autologin_tips NSLocalizedString(@"autologin_tips",nil)
#define skip_text NSLocalizedString(@"skip_text",nil)

/*
 账号选择页面
 */
#define loginhome_button_login NSLocalizedString(@"loginhome_button_login",nil)
#define loginhome_button_playnow NSLocalizedString(@"loginhome_button_playnow",nil)
#define loginhome_tv_split NSLocalizedString(@"loginhome_tv_split",nil)

/*
 账号登录页面
 */
#define btlogin_et_account_hint NSLocalizedString(@"btlogin_et_account_hint",nil)
#define btlogin_et_pwd_hint NSLocalizedString(@"btlogin_et_pwd_hint",nil)
#define btlogin_et_account_null NSLocalizedString(@"btlogin_et_account_null",nil)
#define btlogin_et_pwd_null NSLocalizedString(@"btlogin_et_pwd_null",nil)
#define btlogin_et_loginBtn NSLocalizedString(@"btlogin_et_loginBtn",nil)
#define btlogin_tv_rememberme NSLocalizedString(@"btlogin_tv_rememberme",nil)
#define btlogin_forgetpwd NSLocalizedString(@"btlogin_forgetpwd",nil)
#define btlogin_signup NSLocalizedString(@"btlogin_signup",nil)
#define btlogin_noAccount NSLocalizedString(@"btlogin_noAccount",nil)

/**
 游客隐私提示页面
 */
#define guest_register_notice_tv_tips NSLocalizedString(@"guest_register_notice_tv_tips",nil)
#define guest_register_notice_bt_config NSLocalizedString(@"guest_register_notice_bt_config",nil)
#define guest_register_tv_protocol NSLocalizedString(@"guest_register_tv_protocol",nil)
#define guest_register_tv_privacy NSLocalizedString(@"guest_register_tv_privacy",nil)

/**提示游客绑定*/
#define notice_bind_bt_config NSLocalizedString(@"notice_bind_bt_config",nil)
#define notice_later_bt_config NSLocalizedString(@"notice_later_bt_config",nil)
#define notice_tv_remind NSLocalizedString(@"notice_tv_remind",nil)
#define notice_tv_title NSLocalizedString(@"notice_tv_title",nil)
#define notice_bt_config NSLocalizedString(@"notice_bt_config",nil)


/**注册页面*/
#define register_tv_title NSLocalizedString(@"register_tv_title",nil)
#define register_et_account_hint NSLocalizedString(@"register_et_account_hint",nil)
#define register_tips_account_null NSLocalizedString(@"register_tips_account_null",nil)
#define register_tips_account_reg NSLocalizedString(@"register_tips_account_reg",nil)
#define register_et_pwd_hint NSLocalizedString(@"register_et_pwd_hint",nil)
#define register_tips_pwd_null NSLocalizedString(@"register_tips_pwd_null",nil)
#define register_tips_pwd_reg NSLocalizedString(@"register_tips_pwd_reg",nil)
#define register_et_pwdcf_hint NSLocalizedString(@"register_et_pwdcf_hint",nil)
#define register_tips_pwdcf_null NSLocalizedString(@"register_tips_pwdcf_null",nil)
#define register_tips_pwd_pwdcf NSLocalizedString(@"register_tips_pwd_pwdcf",nil)
#define register_tv_protocol NSLocalizedString(@"register_tv_protocol",nil)
#define register_tv_privacy NSLocalizedString(@"register_tv_privacy",nil)
#define register_tv_fb NSLocalizedString(@"register_tv_fb",nil)
#define register_tv_gp NSLocalizedString(@"register_tv_gp",nil)
#define register_tv_comfrom NSLocalizedString(@"register_tv_comfrom",nil)
#define register_protocol_url NSLocalizedString(@"register_protocol_url",nil)
#define register_privacy_url NSLocalizedString(@"register_privacy_url",nil)

/*找回密码-1*/
#define obtainbindemail_et_account_hint NSLocalizedString(@"obtainbindemail_et_account_hint",nil)
#define obtainbindemail_tips_null NSLocalizedString(@"obtainbindemail_tips_null",nil)
#define obtainbindemail_tv_notice NSLocalizedString(@"obtainbindemail_tv_notice",nil)
#define obtainbindemail_bt_config NSLocalizedString(@"obtainbindemail_bt_config",nil)

/*通过邮箱重置密码-2*/
#define changpwdbyemail_notice_tv_tips NSLocalizedString(@"changpwdbyemail_notice_tv_tips",nil)
#define changpwdbyemail_et_code_hint NSLocalizedString(@"changpwdbyemail_et_code_hint",nil)
#define changpwdbyemail_et_pwd_hint NSLocalizedString(@"changpwdbyemail_et_pwd_hint",nil)
#define changpwdbyemail_tips_null NSLocalizedString(@"changpwdbyemail_tips_null",nil)
#define changpwdbyemail_tips_newpwd_reg NSLocalizedString(@"changpwdbyemail_tips_newpwd_reg",nil)
#define changpwdbyemail_bt_config NSLocalizedString(@"changpwdbyemail_bt_config",nil)
#define changpwdbyemail_bt_sendcode_config NSLocalizedString(@"changpwdbyemail_bt_sendcode_config",nil)
#define tips_email_check NSLocalizedString(@"tips_email_check",nil)
#define relogin_notice_tv_tips NSLocalizedString(@"relogin_notice_tv_tips",nil)
#define relogin_notice_bt_config NSLocalizedString(@"relogin_notice_bt_config",nil)

/**用户通过页面修改密码*/
#define changepwd_tv_title NSLocalizedString(@"changepwd_tv_title",nil)
#define changepwd_et_oldpwd_hint NSLocalizedString(@"changepwd_et_oldpwd_hint",nil)
#define changepwd_et_newpwd_hint NSLocalizedString(@"changepwd_et_newpwd_hint",nil)
#define changepwd_bt_config NSLocalizedString(@"changepwd_bt_config",nil)
#define changepwd_tips_null NSLocalizedString(@"changepwd_tips_null",nil)
#define changepwd_tips_pwd_newpwd NSLocalizedString(@"changepwd_tips_pwd_newpwd",nil)
#define changepwd_tips_newpwd_reg NSLocalizedString(@"changepwd_tips_newpwd_reg",nil)
#define changepwd_tips_newpwd_newpwdcf NSLocalizedString(@"changepwd_tips_newpwd_newpwdcf",nil)
#define changepwd_tips_changed NSLocalizedString(@"changepwd_tips_changed",nil)
#define changepwd_tips_oldpwd_reg NSLocalizedString(@"changepwd_tips_oldpwd_reg",nil)


/**绑定邮箱*/
#define bindmail_tv_notice_account NSLocalizedString(@"bindmail_tv_notice_account",nil)
#define bindmail_tv_notice NSLocalizedString(@"bindmail_tv_notice",nil)
#define bindmail_et_email_hint NSLocalizedString(@"bindmail_et_email_hint",nil)
#define bindmail_et_email_code_hint NSLocalizedString(@"bindmail_et_email_code_hint",nil)
#define bindmail_bt_config NSLocalizedString(@"bindmail_bt_config",nil)
#define bindmail_bt_getcode_config NSLocalizedString(@"bindmail_bt_getcode_config",nil)
#define bindmail_tips_mail_reg NSLocalizedString(@"bindmail_tips_mail_reg",nil)
#define bindmail_tips_mailCode NSLocalizedString(@"bindmail_tips_mailCode",nil)
#define bindmail_tips_tomail NSLocalizedString(@"bindmail_tips_tomail",nil)
#define bindmail_tips_bindsuc NSLocalizedString(@"bindmail_tips_bindsuc",nil)

/**FAQ*/
#define faq_tv_title NSLocalizedString(@"faq_tv_title",nil)

/*用户中心*/
#define usercenter_bt_bindemail_config NSLocalizedString(@"usercenter_bt_bindemail_config",nil)
#define usercenter_bt_changepwd_config NSLocalizedString(@"usercenter_bt_changepwd_config",nil)
#define usercenter_bt_bindaccount_config NSLocalizedString(@"usercenter_bt_bindaccount_config",nil)
#define usercenter_bt_switch_config NSLocalizedString(@"usercenter_bt_switch_config",nil)
#define usercenter_tv_ub_account_info NSLocalizedString(@"usercenter_tv_ub_account_info",nil)
#define usercenter_tv_ub_accountname_info NSLocalizedString(@"usercenter_tv_ub_accountname_info",nil)
#define usercenter_tv_third_account_info NSLocalizedString(@"usercenter_tv_third_account_info",nil)
#define usercenter_tv_ub_account_notice NSLocalizedString(@"usercenter_tv_ub_account_notice",nil)
#define usercenter_tv_notice_fb NSLocalizedString(@"usercenter_tv_notice_fb",nil)
#define usercenter_tv_notice_gp NSLocalizedString(@"usercenter_tv_notice_gp",nil)
#define usercenter_tv_notice_guest NSLocalizedString(@"usercenter_tv_notice_guest",nil)
#define usercenter_tv_notice_kk NSLocalizedString(@"usercenter_tv_notice_kk",nil)
#define usercenter_tv_bound_fb NSLocalizedString(@"usercenter_tv_bound_fb",nil)
#define usercenter_tv_bound_gp NSLocalizedString(@"usercenter_tv_bound_gp",nil)
#define usercenter_tv_bound_guest NSLocalizedString(@"usercenter_tv_bound_guest",nil)
#define usercenter_tv_bound_kk NSLocalizedString(@"usercenter_tv_bound_kk",nil)
#define usercenter_bt_support_config NSLocalizedString(@"usercenter_bt_support_config",nil)
#define usercenter_tips_email_bound NSLocalizedString(@"usercenter_tips_email_bound",nil)
#define usercenter_bt_support_config NSLocalizedString(@"usercenter_bt_support_config",nil)
#define login_Frist NSLocalizedString(@"login_Frist",nil)



typedef enum{
    UBee_SHARE_IMG = 1,
    UBee_SHARE_WEB =2,
    UBee_SHARE_TEXT
}UBee_ShareType;

@interface UBeeInfoSettingUtil : UBeeJoyBasePostData

+ (NSMutableDictionary *)getUBeeBundleInfoPlistParamers;

+ (NSString *)getAccountUrl;

+ (NSString *)getSdkVersion;

+ (NSString *)getUBeeAppId;

+ (NSString *)getUBeeMiniChannelId;

+ (NSString *)getLangType;

+ (NSString *)getUBeePackageId;

+ (NSString * )getUBeeDataAppId;

+ (NSString * )getUBeePlatformId;

+ (NSString *)getUbeeBundlePath;

+ (BOOL)isAutoLoginByInfoPlist;

+ (NSString *)getPaymentUrl;

+ (NSString *)getPolicyUrl;

+ (NSString *)getUserPrivatePolicyUrl;

+ (NSDictionary *)getEventTrackToken;

+ (BOOL)getAppEventTrackSwitch;
+ (BOOL)getFBEventTrackSwitch;
+ (BOOL)getAdjustEventTrackSwitch;
+ (NSString *)getAdjustAppToken;
+ (BOOL)getAdjustAppLog;
+ (NSString *)getStoreReviewUrl;
+ (NSString *)getCurrencyCode;
+ (NSString *)getFaceBookDisplayName;
+ (NSString *)getGoogleClinetID;
+ (NSString *)getFAQUrl;

+ (BOOL)getPromissionViewSwitch;

#pragma mark adjustAppSecret的获取
+ (NSUInteger)getAdjustAppSecret;
+ (NSUInteger)getAdjustAppSecretInfo1;
+ (NSUInteger)getAdjustAppSecretInfo2;
+ (NSUInteger)getAdjustAppSecretInfo3;
+ (NSUInteger)getAdjustAppSecretInfo4;

@end
