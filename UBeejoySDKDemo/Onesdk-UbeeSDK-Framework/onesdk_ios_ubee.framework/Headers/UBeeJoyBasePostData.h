//
//  UBeeJoyBasePostData.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/4.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import <onesdk_ios_seacommon/NSObject+YYModel.h>


@interface UBeeJoyBasePostData : NSObject

@property(nonatomic, assign)int appId ;
@property(nonatomic, strong)Device *device;//手机型号

@end
