//
//  UBeeResponseData.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/5.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <onesdk_ios_seacommon/NSObject+YYModel.h>
#import "UBeeApiResult.h"
@interface UBeeResponseData : NSObject

@property(nonatomic, strong)UBeeApiResult *res;

@end
