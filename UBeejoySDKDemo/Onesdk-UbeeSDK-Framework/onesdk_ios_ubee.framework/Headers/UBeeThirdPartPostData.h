//
//  UBeeThirdPartPostData.h
//  onesdk-ios-ubee
//
//  Created by Tanjiatao on 2017/12/5.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UBeeJoyBasePostData.h"
#import "Args.h"
@interface UBeeThirdPartPostData : UBeeJoyBasePostData

@property(nonatomic, strong)NSString *identity;
@property(nonatomic, strong)NSString *thirdToken;
@property(nonatomic, strong)Args *extArgs;
@property(nonatomic, assign)int thirdId;
@property(nonatomic, strong)NSString *thirdUserId;

@end
