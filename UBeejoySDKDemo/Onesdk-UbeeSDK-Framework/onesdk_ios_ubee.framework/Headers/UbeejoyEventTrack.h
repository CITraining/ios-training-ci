//
//  BaitianEventTrack.h
//  BaitianEventTrack
//
//  Created by Tanjiatao on 2017/4/28.
//  Copyright © 2017年 Baitian. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const Event_Begin_Init =@"500121" ;//SDK开始初始化
static NSString * const Event_Init_Finish =@"500122" ;//SDK初始化成功
static NSString * const Event_Open_HomeLoginPage =@"500123" ;//登录首页成功打开
static NSString * const Event_FB_Shouquan =@"500124" ;//请求FB授权
static NSString * const Event_FB_Shouquan_Fail =@"500125" ;//FB授权失败
static NSString * const Event_FB_Shouquan_Success =@"500126" ;//FB成功授权
static NSString * const Event_FB_AutoLogin =@"500127" ;//FB账号进入自动登录
static NSString * const Event_FB_Login_Success =@"500128" ;//FB成功登录
static NSString * const Event_GP_Shouquan =@"500129" ;//请求GP授权
static NSString * const Event_GP_Shouquan_Fail =@"500130" ;//GP授权失败
static NSString * const Event_GP_Shouquan_Success =@"500131" ;//GP成功授权
static NSString * const Event_GP_AutoLogin =@"500132" ;//GP账号进入自动登录
static NSString * const Event_GP_Login_Success =@"500133" ;//GP账号登录成功
static NSString * const Event_Open_ResigterPage =@"500134" ;//注册页面成功打开
static NSString * const Event_Resigter_Success =@"500135" ;//注册成功
static NSString * const Event_Open_Accountpage =@"500136" ;//帐号登录页面成功打开
static NSString * const Event_Accountpage_Success =@"500137" ;//帐号登录成功
static NSString * const Event_Accountpage_Fail =@"500138" ;//帐号登录失败
static NSString * const Event_GotoStore =@"500139" ;//前往商店评价
static NSString * const Event_GotoStore_Fail =@"500140" ;//不前往商店评价
static NSString * const Event_EnterGame =@"500231" ;//进入区服
static NSString * const Event_CreateRole =@"500230" ;//创建角色

//游客绑定
static NSString * const Event_VisitorBindFbId =@"500289" ;//游客开始绑定FB
static NSString * const Event_VisitorBindFbIdSuccessed =@"500290" ;//FB授权游客绑定成功
static NSString * const Event_VisitorBindFbIdServerSuccessed =@"500291" ;//后端返回绑定成功
static NSString * const Event_VisitorBindFbIdServerFail =@"500292" ;//后端返回绑定失败

static NSString * const Event_VisitorBindFbId_KEY_VisitorUserId =@"visitorUserId" ;
static NSString * const Event_VisitorBindFbId_KEY_VisitorIdentity =@"visitorIdentity" ;
static NSString * const Event_VisitorBindFbId_KEY_VisitorAccount =@"visitorAccount" ;
static NSString * const Event_VisitorBindFbId_KEY_FacebookUserID =@"facebookUserId" ;
static NSString * const Event_VisitorBindFbId_KEY_BindUserId =@"bindUserId" ;//绑定成功后的用户ID
static NSString * const Event_VisitorBindFbId_KEY_FailCode =@"failCode" ;//绑定成功后的用户ID
static NSString * const Event_VisitorBindFbId_KEY_FailMsg =@"failMsg" ;//绑定成功后的用户ID

//分享结果统计
static NSString * const Event_ShareActionResult =@"500319" ;


@interface UbeejoyEventTrack : NSObject

+(UbeejoyEventTrack *)shareInstance;

-(void)trackEvent:(NSString *)eventId extDictionary:(NSMutableDictionary *)extDictionary;

-(void)trackAccountEvent:(NSString *)eventId accountId:(NSString *)accountId
           extDictionary:(NSMutableDictionary *)extDictionary;



@end
