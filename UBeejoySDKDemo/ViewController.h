//
//  ViewController.h
//  UBeejoySDKDemo
//
//  Created by Tanjiatao on 2018/9/30.
//  Copyright © 2018年 Tanjiatao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (weak,nonatomic) IBOutlet UILabel *sdkInitresult;
@property (weak,nonatomic) IBOutlet UILabel *sdkLoginresult;
@property (weak,nonatomic) IBOutlet UILabel *sdkPayresult;

-(IBAction) sdkInit:(id)sender;
-(IBAction) sdkLogin:(id)sender;
-(IBAction) sdkPay:(id)sender;
-(IBAction) sdkCreateRole:(id)sender;
-(IBAction) sdkEnterGame:(id)sender;
-(IBAction) sdkUpgradRole:(id)sender;
-(IBAction) sdkFinishNewRoleTutorial:(id)sender;
-(IBAction) sdkObtainNewRolePack:(id)sender;
-(IBAction) sdkEventTrack:(id)sender;
-(IBAction) sdkSocialShare:(id)sender;
-(IBAction) sdkShareResultTrack:(id)sender;
-(IBAction) sdkRateUs:(id)sender;
-(IBAction) sdkUserCenter:(id)sender;
-(IBAction) sdkLogout:(id)sender;
@end

