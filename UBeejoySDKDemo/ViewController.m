//
//  ViewController.m
//  UBeejoySDKDemo
//
//  Created by Tanjiatao on 2018/9/30.
//  Copyright © 2018年 Tanjiatao. All rights reserved.
//

#import "ViewController.h"
#import <OnesdkBaitianFramework/OnesdkBaitianFramework.h>
#import <onesdk_ios_ubee/SocialManager.h>
#import <onesdk_ios_seacommon/OneSDKCommonSDKUtil.h>
#import <onesdk_ios_seacommon/OneSDKCommonStringUtil.h>
#import <onesdk_ios_ubee/AppEventManager.h>
@interface ViewController ()<SocialShareDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //注销账号的回调
    [[BTsdkManager shareSDK]setLogoutBlock:^(NSDictionary *dicinfo) {
        DebugLog(@"注销成功，回调登录游戏界面");
    } failure:^(NSDictionary *dicinfo) {
        DebugLog(@"注销失败，继续游戏");
    }];
    //社交分享的回调
    [[SocialManager shareSDKInstance]setShareDelegate:self];
}

- (void)onSocialShareSuccess:(NSString *)platform{
    //该结果为第三方分享商返回的分享完成的接口，分享完成并一定就是分享成功，如FB跳转到App分享界面就会返回该回调
    
    //每次分享完成需要记录分享行为   ---《单抽分享》为进行单次抽卡分享成功的行为，shareResult： 1代表成功，0为分享失败
    [[BTsdkManager shareSDK]reportShareResult:@"FB" shareDes:@"单抽分享" shareResult:1];
    
}
- (void)onSocialShareCancel:(NSString *)platform{
    //取消分享
     [[BTsdkManager shareSDK]reportShareResult:@"FB" shareDes:@"单抽分享" shareResult:0];
}
- (void)onSocialShareError:(NSString *)platform{
    //分享出错
     [[BTsdkManager shareSDK]reportShareResult:@"FB" shareDes:@"单抽分享" shareResult:0];;
}


-(IBAction) sdkInit:(id)sender{
    //初始化之前要设置注销和分享的回调
    [[BTsdkManager shareSDK]initSDK:^(NSDictionary *dicinfo) {
        DebugLog(@"初始化成功");
        self.sdkInitresult.text = @"初始化成功";
    } failure:^(NSDictionary *dicinfo) {
        DebugLog(@"初始化失败，提示玩家再次进行初始化");
        self.sdkInitresult.text = @"初始化失败";
    }];
}
-(IBAction) sdkLogin:(id)sender{
    [[BTsdkManager shareSDK] loginSDK:^(NSDictionary *dicinfo) {
        NSString *sessionId = [dicinfo valueForKey:@"sessionId"];
        NSString *reslut =[NSString stringWithFormat:@"登录成功 sessionId = %@",sessionId];
        NSString *platformId = [dicinfo valueForKey:@"platformId"];
        //获取sessionID之后 需要通过sessionID获取用户详细信息,游戏服务端需要将sessionID发至sdk服务器进行验证
        self.sdkLoginresult.text = [NSString stringWithFormat:@"Id:%@",[[BTsdkManager shareSDK]getCurrentUserId] ];
    } failure:^(NSDictionary *dicinfo) {
       //登录失败，弹框提示玩家，是否进行再次登录。
       NSString *resultCode = [dicinfo valueForKey:BT_RESULTCODE];//错误码
       DebugLog(@"resultCode = %@",resultCode);
       NSString *errorMsg = [dicinfo valueForKey:BT_RESULTDESC];//错误详情
       DebugLog(@"errorMsg = %@",errorMsg);
        self.sdkLoginresult.text = @"登录失败";
    }];
}
-(IBAction) sdkPay:(id)sender{
    //支付与包名及商品ID 有关，未配置商品ID会出现下单失败，请看FAQ
        BTSdkPaymentInfo *paymentInfo = [[BTSdkPaymentInfo alloc]init];
        [paymentInfo setRoleId:@"36357"];
        [paymentInfo setRoleName:@"神一样的对手"];
        [paymentInfo setRoleLevel:@"55"];
        [paymentInfo setServerId:@"1"];
        [paymentInfo setServerName:@"苹果英超"];
        [paymentInfo setMoney:@"99.99"];
        [paymentInfo setCallBackInfo:@"透传字段"];
        [paymentInfo setOutOrderNo:@"PI_KR_KJB_2250_12323"];
        [paymentInfo setFilterParam:@"mqfilter"];
        [paymentInfo setAppproductId:@"ubj.cellkr.100usd"];//把这个苹果的id存入appproducetId中 但不上传后台
        [paymentInfo setGoodsId:@"PI_KR_KJB_2250"];
        [paymentInfo setZoneId:@"3"];
        [paymentInfo setGoodsDesc:@"60钻石"];//商品说明
        [[BTsdkManager shareSDK] payWithPaymentInfo:paymentInfo success:^(NSDictionary *dicinfo) {
            //支付流程结束
            self.sdkPayresult.text = @"交易结束";
        } failure:^(NSDictionary *dicinfo) {
            //支付失败
            NSString *resultCode = [dicinfo valueForKey:BT_RESULTCODE];//错误码
            NSString *errorMsg = [dicinfo valueForKey:BT_RESULTDESC];//错误详情
            self.sdkPayresult.text = errorMsg;
        }];
}

-(IBAction) sdkCreateRole:(id)sender{
    NSMutableDictionary *roleInfo = [NSMutableDictionary dictionary];
    [roleInfo setValue:@"极速大神" forKey:@"roleName"];//角色名称
    [roleInfo setValue:@"1023" forKey:@"roleId"];//角色id
    [roleInfo setValue:@"9" forKey:@"roleLevel"];//角色等级
    [roleInfo setValue:@"1" forKey:@"serverId"];//游戏服务器id
    [roleInfo setValue:@"百田1服" forKey:@"serverName"];//游戏服务器名称
    
    [roleInfo setValue:@"1515131678" forKey:@"roleCTime"];//角色创建时间 10位时间戳 新增
    [roleInfo setValue:@"1" forKey:@"isVip"];//角色是否Vip 1为是 新增
    [roleInfo setValue:@"3" forKey:@"vipLevel"];//角色vip等级 新增
    [roleInfo setValue:@"1515131789" forKey:@"loginTime"];//角色登录区服的服务器时间 10位时间戳（当创建角色的时候 该字段可以不传
    [[BTsdkManager shareSDK]createRole:roleInfo];
}

-(IBAction) sdkEnterGame:(id)sender{
    NSMutableDictionary *roleInfo = [NSMutableDictionary dictionary];
    [roleInfo setValue:@"极速大神" forKey:@"roleName"];//角色名称
    [roleInfo setValue:@"1023" forKey:@"roleId"];//角色id
    [roleInfo setValue:@"9" forKey:@"roleLevel"];//角色等级
    [roleInfo setValue:@"1" forKey:@"serverId"];//游戏服务器id
    [roleInfo setValue:@"百田1服" forKey:@"serverName"];//游戏服务器名称
    
    [roleInfo setValue:@"1515131678" forKey:@"roleCTime"];//角色创建时间 10位时间戳 新增
    [roleInfo setValue:@"1" forKey:@"isVip"];//角色是否Vip 1为是 新增
    [roleInfo setValue:@"3" forKey:@"vipLevel"];//角色vip等级 新增
    [[BTsdkManager shareSDK]enterGame:roleInfo];
}

-(IBAction) sdkUpgradRole:(id)sender{
    NSMutableDictionary *roleInfo = [NSMutableDictionary dictionary];
    [roleInfo setValue:@"极速大神" forKey:@"roleName"];//角色名称
    [roleInfo setValue:@"1023" forKey:@"roleId"];//角色id
    [roleInfo setValue:@"9" forKey:@"roleLevel"];//角色等级
    [roleInfo setValue:@"1" forKey:@"serverId"];//游戏服务器id
    [roleInfo setValue:@"百田1服" forKey:@"serverName"];//游戏服务器名称
    
    [roleInfo setValue:@"1515131678" forKey:@"roleCTime"];//角色创建时间 10位时间戳 新增
    [roleInfo setValue:@"1" forKey:@"isVip"];//角色是否Vip 1为是 新增
    [roleInfo setValue:@"3" forKey:@"vipLevel"];//角色vip等级 新增
    [roleInfo setValue:@"1515131789" forKey:@"loginTime"];//角色登录区服的服务器时间 10位时间戳（当创建角色的时候 该字段可以不传
    [[BTsdkManager shareSDK]upgradRole:roleInfo];
}
-(IBAction) sdkFinishNewRoleTutorial:(id)sender{
    [[BTsdkManager shareSDK]onFinishNewRoleTutorial];
}
-(IBAction) sdkObtainNewRolePack:(id)sender{
    [[BTsdkManager shareSDK]onObtainNewRolePack];
}
-(IBAction) sdkEventTrack:(id)sender{
    NSMutableDictionary * eventProperties = [[NSMutableDictionary alloc]init];
    [eventProperties setValue:@"Network is bad" forKey:@"myError"];//错误信息 已myError字段记录到数据平台
    [eventProperties setValue:@"100014" forKey:@"dataAppId"];//错误的key值属性，dataAppId为保留字段，会影响统计结果。
    [[SaTrackManager shareInstance]sensorsTrack:@"500001" withProperties:eventProperties isFlush:YES];//500001为SDK的数据后台分配的事件ID
}
-(IBAction) sdkSocialShare:(id)sender{
    //进行网页分享 当传入的shareUrl不为空的话，FB的分享默认为分享网页。
    ShareObject *shareObject = [[ShareObject alloc]init];
    shareObject.shareUrl = @"http://www.172tt.com/jszb/";
    shareObject.sharePlatform = UBee_SharePlatForm_FB;
    [[SocialManager shareSDKInstance]share:shareObject];
    
    
    //进行图片分享 当传入的shareUrl为空并且imgPath传入了图片在手机上的路径，则进行图片分享
//    ShareObject *shareObject = [[ShareObject alloc]init];
//    shareObject.shareUrl = @"";
//    shareObject.imgPath = @"/var/mobile/cContainers/Data/Application/.../.../test.png";//图片在手机中保存的路径
//    shareObject.sharePlatform = UBee_SharePlatForm_FB;
//    [[SocialManager shareSDKInstance]share:shareObject];
}
-(IBAction) sdkShareResultTrack:(id)sender{
    
}
-(IBAction) sdkRateUs:(id)sender{
    [[BTsdkManager shareSDK]rateUS];
}
-(IBAction) sdkUserCenter:(id)sender{
    //目前无效果
    [[BTsdkManager shareSDK]  showUsercenter];

    
    
    
    
}
-(IBAction) sdkLogout:(id)sender{
     [[BTsdkManager shareSDK]logoutSDK];
}

//其他帮助类方法
-(void)sdkUtil{
    //获取当前账号ID
    NSString *currentId = [[BTsdkManager shareSDK]getCurrentUserId];
    //获取设备信息,用于服务器上传数据使用 返回json格式字符串
    NSString *deviceJson = [[BTsdkManager shareSDK]getDeviceProperties];
    //获取当前游戏ID
    [[BTsdkManager shareSDK]getGameID];
    //获取当前sdk渠道
     int platFormId = [BTsdkManager getPlatFormId];
    
    //判断是否ipad 以下方法需要引入<onesdk_ios_seacommon/OneSDKCommonSDKUtil.h>
    if(isPad){
        
    }
    if([OneSDKCommonSDKUtil isIphoneX]){//是否刘海屏系列手机
        
    }
    
    //以下方法需要引入<onesdk_ios_seacommon/OneSDKCommonStringUtil.h>
    if([OneSDKCommonStringUtil isBlankString:@""]){
        
    }
    //将一个json字符串转为一个字典
    NSDictionary *jsonDic = [OneSDKCommonStringUtil dictionaryWithJsonString:@"jsonString"];
    //将一个字典转换成一个json字符串
    NSString *json = [OneSDKCommonStringUtil dicToJson:jsonDic];
    
    
}


@end
