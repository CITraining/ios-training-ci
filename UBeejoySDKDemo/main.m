//
//  main.m
//  UBeejoySDKDemo
//
//  Created by Tanjiatao on 2018/9/30.
//  Copyright © 2018年 Tanjiatao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
