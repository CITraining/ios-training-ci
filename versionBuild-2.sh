#!/bin/sh

#SDK的版本说明
SDKVersionDesc=CI设置ubeejoy777的版本号

#UBeejoySDK的版本
UBeeJoySDKVersion=CI-7.7.7

WRK_DIR=build
SRCROOT='.'

version='CI-7777-ubeejoy'

#ubeejoy
UBeeJoyBundleName=UBeeJoy.bundle
UBeeJoyBundleFile=${SRCROOT}/UBeejoySDKDemo/SDKLibs/NoahEnInfo/${UBeeJoyBundleName}
UBeeJoyInfoFile=${UBeeJoyBundleFile}/ubeejoy.plist



#配置Ubeejoy版本号
/usr/libexec/PlistBuddy -c 'Set :UBeeJoySdkVersion '${UBeeJoySDKVersion}'('${version}')'  ${UBeeJoyInfoFile}
/usr/libexec/PlistBuddy -c 'Set :UBeeJoySdkVersionDesc '${SDKVersionDesc}  ${UBeeJoyInfoFile}