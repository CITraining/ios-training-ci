#!/bin/sh
#SDK的版本
OnesdkSDKVersion=2.1.8
#SDK的版本说明
SDKVersionDesc=CI888-设置Onesdk的版本号8888


WRK_DIR=build
SRCROOT='.'

version='CI-8888-onesdk'

#onesdk
OnesdkBundleName=BaitianSDK.bundle
OnesdkBundleFile=${SRCROOT}/UBeejoySDKDemo/Onesdk/${OnesdkBundleName}
OnesdkInfoFile=${OnesdkBundleFile}/baitiansdkInfo.plist

#配置版本号
/usr/libexec/PlistBuddy -c 'Set :IosSdkVersion '${OnesdkSDKVersion}'('${version}')'  ${OnesdkInfoFile}
/usr/libexec/PlistBuddy -c 'Set :IosSdkVersionDesc '${SDKVersionDesc}  ${OnesdkInfoFile}
